# -----------------------------------------------------------------------------
# qsfparse.py
#
# Parser for QSF.
# -----------------------------------------------------------------------------

import re
import qsflex
import yacc

# Get the token map
tokens = qsflex.tokens
reserved = qsflex.reserved
valid_qddc_tokens = qsflex.valid_qddc_tokens

# translation-unit:

def p_qsf(p):
	'''qsf : HQSF SCONST
		| qsf include
		| qsf encode
		| qsf dclib
		| qsf define
		| qsf const
		| qsf interface
		| qsf definitions
		| qsf indefinitions
		| qsf hardreq
		| qsf softreq'''
#		| qsf main'''


def p_empty(p):
	'''empty :'''


def p_neg_or_nonneg(p):
	'''nneg : empty
		| NEG'''


def p_id_exp_num(p):
	'''id_exp_num : id
		      | expression
		      | number'''


def p_exp_num(p):
	'''exp_num : expression
		   | number'''


def p_dc_ldelim(p):
	'''dc_ldelim : LT 
		     | LBRACKET 
		     | DLBRACKET 
		     | DLBRACE
	G_or_F : BOX
	       | DIAMOND
	dc_ldelim2 : dc_ldelim
		   | G_or_F'''


def p_dc_rdelim(p):
	'''dc_rdelim : GT 
		     | RBRACKET 
		     | DRBRACKET 
		     | DRBRACE'''


#def p_main(p):
#	'''main : main_type LBRACE main_body RBRACE
#		| main main_type LBRACE main_body RBRACE
#	main_type : INFERG
#		  | INFERF
#		  | INFERGF
#		  | INFERFG'''
#
##def p_header(p):
##	'''header : spec
##		  | header spec'''
#
#
#def p_main_body(p):
#	'''main_body : ind
#		     | softreq 
#		     | assume
#		     | req
#		     | for
#		     | if
#		     | printf
#		     | main_body ind	
#		     | main_body softreq 
#		     | main_body assume	
#		     | main_body req
#		     | main_body for
#		     | main_body if
#		     | main_body printf'''
#

def p_withall(p):
	'''withall : WITH
		   | WITHPAR'''


#def p_ind(p):
#	'''ind : IND id COLON qddc_formula SEMI
#	       | IND id COLON with_statements2''' 


#def p_assume(p):
#	'''assume : ASSUME qddc_formula2 SEMI'''
#
#
#def p_req(p):
#	'''req : REQ qddc_formula2 SEMI'''
#

def p_qddc_formula2(p):
	'''qddc_formula2 : qddc_formula
	    	         | function2
		         | qddc_formula2 logical_bop2 qddc_formula2
		         | LPAREN qddc_formula2 RPAREN
	function2 : ID LPAREN RPAREN 
		  | ID LPAREN params RPAREN '''


def p_for(p):
	'''for : FOR LPAREN multi_assign SEMI logical_exp SEMI multi_assign RPAREN statements
	       | FOR LPAREN multi_assign SEMI logical_exp SEMI multi_assign RPAREN LBRACE statements RBRACE'''


def p_with(p):
	'''with : withall LPAREN multi_assign SEMI logical_exp SEMI multi_assign SEMI qddc_formula_post connector qddc_formula_post RPAREN with_statements2 
	softreq_with : withall LPAREN multi_assign SEMI logical_exp SEMI multi_assign SEMI SEMI softreq_with_exp SEMI SEMI RPAREN softreq_statements2
	softreq_colon_with : withall LPAREN multi_assign SEMI logical_exp SEMI multi_assign SEMI SEMI softreq_with_exp SEMI SEMI RPAREN softreq_colon_statements2
	with_statements2 : with_statements 
			 | LBRACE with_statements RBRACE
	softreq_statements2 : softreq_statements
		            | LBRACE softreq_statements RBRACE
	softreq_colon_statements2 : softreq_statements_colon
		            | LBRACE softreq_statements_colon RBRACE
	softreq_with_exp : empty
			 | DGT'''


def p_if(p):
	'''if : IF LPAREN logical_exp RPAREN LBRACE statements RBRACE elif
	      | IF LPAREN logical_exp RPAREN statements elif
	  if2 : IF LPAREN logical_exp RPAREN LBRACE with_statements RBRACE elif2
	      | IF LPAREN logical_exp RPAREN with_statements elif2
	  softreq_if : IF LPAREN logical_exp RPAREN LBRACE softreq_statements RBRACE softreq_elif
	             | IF LPAREN logical_exp RPAREN softreq_statements softreq_elif'''


def p_elif(p):
	'''elif : else
                | ELIF LPAREN logical_exp RPAREN LBRACE statements RBRACE elif
	        | ELIF LPAREN logical_exp RPAREN statements elif
	  elif2 : else2
                | ELIF LPAREN logical_exp RPAREN LBRACE with_statements RBRACE elif
	        | ELIF LPAREN logical_exp RPAREN with_statements elif2
	  softreq_elif : softreq_else
                       | ELIF LPAREN logical_exp RPAREN LBRACE softreq_statements RBRACE softreq_elif
	               | ELIF LPAREN logical_exp RPAREN softreq_statements softreq_elif'''


def p_else(p):
	'''else : empty
		| ELSE LBRACE statements RBRACE
	        | ELSE statements
	  else2 : empty
		| ELSE LBRACE with_statements RBRACE
	        | ELSE with_statements
	  softreq_else : empty
		       | ELSE LBRACE softreq_statements RBRACE
	               | ELSE softreq_statements'''


def p_connector(p):
	'''connector : SEMI
		     | dc_bop SEMI'''


def p_statements(p):
	'''statements : empty
		      | for
		      | with
		      | if
		      | indicator
		      | input
		      | output
		      | printf
		      | statements for
		      | statements with
		      | statements if
		      | statements indicator
		      | statements input
		      | statements output
		      | statements printf'''
	

def p_with_statements(p):
	'''with_statements : with
			   | if2
			   | printf
			   | qddc_formula_post_ne SEMI 
			   | with_statements with 
			   | with_statements if2 
			   | with_statements printf 
			   | with_statements qddc_formula_post_ne SEMI''' 


def p_fname(p):
	'''fname : ID
		 | DC
		 | fname DOT ID
		 | fname DOT DC'''


def p_include(p):
	'''include : HINCLUDE SCONST
		   | HINCLUDE LT fname GT'''
 

def p_dclib(p):
	'''dclib : HDCLIB SCONST
		 | HDCLIB LT fname GT'''


def p_encode(p):
	'''encode : ENCODE ID LBRACE encode_var SEMI RBRACE
	encode_var : encode_id
		   | encode_with
	encode_with : encode_with2 id EQUALS expression
	encode_with2 : WITH LPAREN multi_assign SEMI rel_exp SEMI multi_assign SEMI SEMI SEMI SEMI RPAREN 
		     | encode_with2 WITH LPAREN multi_assign SEMI rel_exp SEMI multi_assign SEMI SEMI SEMI SEMI RPAREN 
	encode_id : id EQUALS expression
		  | encode_id COMMA id EQUALS expression'''
 

def p_define(p):
	'''define : HDEFINE ID NUMBER
		  | HDEFINE ID SCONST'''
 

def p_const(p):
	'''const : CONST const_list SEMI
	const_list : const_list COMMA ID EQUALS number 
                   | ID EQUALS number'''


def p_inteface(p):
	'''interface : INTERFACE LBRACE interface_section RBRACE'''


def p_interface_section(p):
	'''interface_section : empty
			     | for 
			     | indicator 
			     | input 
			     | output 
			     | constant 
			     | auxvar 
			     | interface_section for 
			     | interface_section indicator 
			     | interface_section input 
			     | interface_section output 
			     | interface_section constant 
			     | interface_section auxvar''' 


def p_indicator(p):
	'''indicator : INDICATOR indicator_list SEMI
	indicator_list : indicator_var 
		       | indicator_list COMMA indicator_var
	indicator_var : id
	              | id VISIBLE
                      | id INVISIBLE'''


def p_input(p):
	'''input : INPUT input_list SEMI
	input_list : id 
		   | id EQUALS exp_num
		   | input_list COMMA id
		   | input_list COMMA id EQUALS exp_num'''


def p_array(p):
	'''array : LBRACKET expression RBRACKET
		 | array LBRACKET expression RBRACKET'''


def p_id(p):
	'''id : ID
	      | XTDVAL	
	      | ID array	
	      | XTDVAL array'''	


def p_output(p):
	'''output : OUTPUT output_list SEMI
	output_list : output_var
		    | output_list COMMA output_var
	output_var : id
		   | id MONITOR ID
		   | id MONITOR XTDVAL
		   | id EQUALS exp_num 
		   | id EQUALS exp_num MONITOR ID
		   | id EQUALS exp_num MONITOR XTDVAL'''

 
def p_constant(p):
	'''constant : CONSTANT constant_list SEMI
	constant_list : ID EQUALS exp_num
		      | constant_list COMMA ID EQUALS exp_num'''
 

def p_auxvar(p):
	'''auxvar : AUXVAR auxvar_list SEMI
	auxvar_list : ID
		    | auxvar_list COMMA ID'''



def p_hardreq(p):
	'''hardreq : HARDREQ LBRACE qddc_formula2 SEMI RBRACE
		   | HARDREQ LBRACE useind qddc_formula2 SEMI RBRACE'''


def p_softreq(p):
	'''softreq : SOFTREQ LBRACE softreq_statements RBRACE 
		   | SOFTREQ LBRACE useind softreq_statements RBRACE 
	softreq_statements : softreq_list 
			   | softreq_statements softreq_list
			   | softreq_statements softreq_with
	softreq_list : softreq_list2 SEMI
		     | softreq_list2_colon SEMI
	softreq_list2 : prop_formula
		     | prop_error
		     | softreq_list2 DGT 
		     | softreq_list2 DGT prop_formula
		     | softreq_list2 DGT prop_error
	softreq_list2_colon : softreq_list3_colon
		     	    | prop_error
		     	    | softreq_list2_colon DGT 
		     	    | softreq_list2_colon DGT softreq_list3_colon
		     	    | softreq_list2_colon DGT prop_error
	softreq_list3_colon : prop_formula COLON number
		            | LPAREN softreq_list3_colon RPAREN
	softreq_list_colon_pre : softreq_list2_colon DGT SEMI 
	softreq_list_colon_post : DGT softreq_list2_colon DGT SEMI 
		          | DGT softreq_list2_colon SEMI 
	softreq_statements_colon : softreq_list2_colon 
			   | softreq_list_colon_pre
			   | softreq_colon_with
		     	   | LBRACE softreq_statements_colon RBRACE
			   | softreq_statements_colon softreq_list2_colon
			   | softreq_statements_colon softreq_list_colon_pre
			   | softreq_statements_colon softreq_list_colon_post
			   | softreq_statements_colon softreq_colon_with'''
					


def p_useind(p):
	'''useind : USEIND useind_list SEMI
	useind_list : ID
		    | useind_list COMMA ID'''



def p_qddc_formula(p):
	'''qddc_formula : LT prop_formula GT
		      |	LBRACKET prop_formula RBRACKET
		      |	DLBRACKET prop_formula DRBRACKET
		      |	DLBRACE prop_formula DRBRACE
		      | LPAREN qddc_formula RPAREN
		      | NEG qddc_formula
		      | BOX qddc_formula
		      | DIAMOND qddc_formula
		      | derived_formula
		      | term
	    	      | function
		      | qddc_formula dc_bop qddc_formula
		      | quantifier id DOT qddc_formula
	quantifier : ALL 
		   | EX'''


def p_split_qddc_formula(p):
	'''qddc_formula_post : SEMI
			     | qddc_formula_post_ne SEMI
	qddc_formula_post_ne : qddc_formula_logical 
			     | qddc_prop_formula3 
			     | qddc_formula_rparen
	qddc_formula_rparen : RPAREN rparen 
			    | RPAREN rparen qddc_formula_logical
			    | RPAREN rparen qddc_rdelim
	qddc_formula_rparen2 : RPAREN
			    | qddc_formula_rparen2 dc_rdelim rparen
			    | qddc_formula_rparen2 logical_bop2 prop_formula rparen
			    | qddc_formula_rparen2 logical_bop2 prop_error
			    | qddc_formula_rparen2 dc_rdelim rparen dc_bop qddc_formula rparen
			    | rparen dc_bop qddc_formula rparen
	qddc_formula_logical : dc_bop
			    | CARET qddc_formula_logical2  
			    | CARET qddc_formula_logical2 dc_bop 
			    | CARET qddc_formula_logical2 dc_bop dc_function_error 
			    | CARET dc_function_error 
			    | logical_bop2 qddc_formula_logical3
	qddc_formula_logical2 : qddc_formula rparen
			    | qddc_formula_logical2 dc_bop qddc_formula rparen
	qddc_formula_logical3 :  prop_error  
			    | qddc_formula_logical4  
			    | qddc_formula_logical4 dc_bop 
			    | qddc_formula_logical4 logical_bop2 prop_error 
			    | qddc_formula_logical4 dc_rdelim rparen  
			    | qddc_formula_logical4 dc_rdelim rparen dc_bop  
			    | qddc_formula_logical4 dc_rdelim rparen dc_bop qddc_formula_logical2 
			    | qddc_formula_logical4 dc_rdelim rparen dc_bop dc_function_error 
			    | qddc_formula_logical2 
			    | dc_function_error 
			    | function_rparen dc_bop qddc_formula_logical2 
			    | function_rparen dc_bop dc_function_error 
	qddc_formula_logical4 : prop_formula rparen 
			    | qddc_formula_logical4 logical_bop2 prop_formula rparen
	qddc_rparen : qddc_formula rparen
		    | qddc_rparen dc_bop qddc_formula rparen
	function_rparen : function rparen
			| function_rparen logical_bop2 function rparen
	rparen : empty
	       | RPAREN
	       | rparen RPAREN  
	qddc_prop_formula3 : qddc_rparen 
			 | prop_rparen 
		         | prop_error
		         | prop_rparen_error
			 | dc_rparen 
			 | comma_function 
		         | lparen_error
		         | dc_function_error
	qddc_rdelim : dc_rdelim rparen
		  | qddc_rdelim dc_bop  
		  | qddc_rdelim dc_bop qddc_rparen 
		  | qddc_rdelim dc_bop qddc_rparen_error  
		  | qddc_rdelim dc_bop dc_function_error 
	dc_rparen : qddc_rdelim
		  | prop_rparen qddc_rdelim
	prop_rparen : prop_formula rparen
		    | prop_rparen logical_bop2 prop_formula rparen
	comma_function : comma_function2
		       | COMMA 
		       | COMMA prop_formula COMMA
		       | COMMA expression COMMA
		       | COMMA prop_formula comma_function
		       | COMMA expression comma_function
	comma_function2 : COMMA params RPAREN rparen
		       | comma_function2 qddc_formula_logical
		       | comma_function2 qddc_rdelim'''
#	prop_comma_function : prop_comma_function2
#		            | prop_comma_function2 logical_bop2 prop_error
#	prop_comma_function2 : comma_function	
#		            | prop_comma_function2 logical_bop2 prop_formula rparen
#		            | prop_comma_function2 logical_bop2 prop_error'''
#			    | qddc_formula_rparen2  
#			    | qddc_formula_rparen2 rparen dc_bop
#			    | qddc_formula_rparen2 rparen dc_rdelim dc_bop dc_function_error
#			    | rparen dc_bop dc_function_error

def p_dc_formula(p):
	'''dc_formula : LT prop_formula GT
		      |	LBRACKET prop_formula RBRACKET
		      |	DLBRACKET prop_formula DRBRACKET
		      |	DLBRACE prop_formula DRBRACE
		      | LPAREN dc_formula RPAREN
		      | NEG dc_formula
		      | BOX dc_formula
		      | DIAMOND dc_formula
		      | derived_formula
		      | term
		      | dc_formula dc_bop dc_formula
	         term :	SLEN dc_rel_op id_exp_num
	              | SCOUNT prop_formula dc_rel_op id_exp_num
		      | SDUR prop_formula dc_rel_op id_exp_num
		      | LPAREN term RPAREN
		derived_formula : TRUE
		      		| FALSE
		      		| PT
		      		| EXT'''


	
def p_prop_formula(p):
	'''prop_formula : id
			| function
			| LPAREN prop_formula RPAREN
			| NEG prop_formula
			| prop_formula logical_bop2 prop_formula
	prop_formula_rparen : RPAREN
			| prop_formula 
			| prop_formula_rparen RPAREN
			| prop_formula_rparen logical_bop2 prop_formula_rparen'''


def p_prod_ldelim_error(p):
	'''lparen_error : LPAREN error
			| LPAREN lparen_error
	error2 : error
	       | lparen_error	
	function_error : FUNCTIONID LPAREN error2
		       | FUNCTIONID LPAREN function_error2	
		       | LPAREN function_error
	function_error2 : expression COMMA error2
		  | prop_formula COMMA error2
		  | expression COMMA function_error2
		  | prop_formula COMMA function_error2
	prop_error : lparen_error
		   | NEG error2
		   | NEG prop_error
		   | LPAREN prop_error
		   | prop_formula error2
		   | prop_formula logical_bop2 error2
		   | prop_formula logical_bop2 function_error
		   | prop_formula logical_bop2 prop_error
	dc_error : dc_ldelim2 error2
		 | dc_ldelim prop_error
		 | dc_ldelim function_error
		 | LPAREN dc_error
  	         | G_or_F dc_error
  	         | qddc_formula error
  	         | qddc_formula dc_bop error2
	qddc_rparen_error : qddc_rparen dc_bop error
		 	  | qddc_rparen dc_bop dc_error
  	         	  | qddc_rparen dc_bop qddc_rparen_error
	prop_rparen_error : prop_rparen logical_bop2 error
			  | prop_rparen logical_bop2 prop_error
		          | prop_rparen logical_bop2 function_error
	dc_function_error : dc_error
			  | function_error'''


def p_function(p):
	'''functions : function
		     | functions logical_bop2 function
	function : FUNCTIONID LPAREN RPAREN 
		 | FUNCTIONID LPAREN params RPAREN 
        params : prop_formula 
	       | qddc_formula 
	       | expression 
	       | prop_formula COMMA params 
	       | qddc_formula COMMA params 
	       | expression COMMA params''' 
#	param : prop_formula
#		| expression'''


def p_definitions(p):
	'''definitions : DEFINITIONS LBRACE spec RBRACE'''


def p_indefinitions(p):
	'''indefinitions : INDEFINITIONS LBRACE ind RBRACE
		     ind : INDEF id COLON qddc_formula SEMI
		     	 | ind INDEF id COLON qddc_formula SEMI'''


def p_spec(p):
	'''spec : implies
		| init
		| anti
		| follows
		| triggers
		| pref
		| ppref
		| dc
		| node
		| spec implies
		| spec init
		| spec anti
		| spec follows
		| spec triggers
		| spec pref
		| spec ppref
		| spec dc
		| spec node'''


def p_implies(p):
	'''implies : IMPLIES FUNCTIONID arg LBRACE speclet speclet RBRACE
		   | HIMPLIES FUNCTIONID arg LBRACE speclet speclet RBRACE'''


def p_init(p):
	'''init : INIT FUNCTIONID arg LBRACE speclet speclet RBRACE
	        | HINIT FUNCTIONID arg LBRACE speclet speclet RBRACE'''


def p_anti(p):
	'''anti : ANTI FUNCTIONID arg LBRACE with_statements RBRACE
	        | HANTI FUNCTIONID arg LBRACE with_statements RBRACE'''


def p_follows(p):
	'''follows : FOLLOWS FUNCTIONID arg LBRACE speclet speclet speclet RBRACE
		   | HFOLLOWS FUNCTIONID arg LBRACE speclet speclet speclet RBRACE'''


def p_triggers(p):
	'''triggers : TRIGGERS FUNCTIONID arg LBRACE speclet speclet speclet RBRACE
                    | HTRIGGERS FUNCTIONID arg LBRACE speclet speclet speclet RBRACE'''


def p_pref(p):
	'''pref : PREF FUNCTIONID arg LBRACE with_statements RBRACE
                | HPREF FUNCTIONID arg LBRACE with_statements RBRACE'''


def p_ppref(p):
	'''ppref : PPREF FUNCTIONID arg LBRACE with_statements RBRACE
                 | HPPREF FUNCTIONID arg LBRACE with_statements RBRACE'''


def p_dc(p):
	'''dc : DC FUNCTIONID arg LBRACE with_statements RBRACE
              | HDC FUNCTIONID arg LBRACE with_statements RBRACE'''


def p_node(p):
	'''node : NODE FUNCTIONID arg LBRACE node_statements RBRACE'''


def p_var_sig(p):
	'''var_sig : empty
		   | var
		   | sig
		   | var sig
		   | sig var
	var_sig_bc : empty
		   | var
		   | sig
		   | bc
		   | var sig
		   | sig var
		   | var bc
		   | sig bc
		   | bc var
		   | bc sig
		   | var sig bc
		   | sig var bc
		   | var bc sig
		   | sig bc var
		   | bc var sig
		   | bc sig var
	var : VAR var_list SEMI
	sig : SIG var_list SEMI
	bc : BC var_list SEMI
	var_list : ID
		 | var_list COMMA ID'''


def p_node_statements(p):
	'''node_statements : var_sig_bc sm'''


def p_sm(p):
	'''sm : SM ID LBRACE var_sig sm_statements RBRACE
	      | SM CONCURRENT ID LBRACE var_sig sm_statements RBRACE
	sm_statements : state
		   | sm_statements state'''


def p_state(p):
	'''state : STATE state_attrib ID LBRACE var_sig state_statements_list RBRACE
		 | STATE state_attrib ID var_sig state_statements_list
	state_statements_except_sm : unless 
			           | until
			           | onentry
			           | onexit
			           | state_body
	state_statements_list_except_sm : state_statements_except_sm
			                | state_statements_list_except_sm state_statements_except_sm
	state_statements_list : state_sm
			      | state_statements_list_except_sm
	state_sm : sm
		 | state_sm sm
	state_body : sm_assignment_list
		   | sm_if
		   | state_body sm_assignment_list
		   | state_body sm_if'''


def p_unless_until(p):
	'''unless : UNLESS LBRACE unless_until_list RBRACE
	          | UNLESS unless_until_list 
	until : UNTIL LBRACE unless_until_list RBRACE
	      | UNTIL unless_until_list 
	unless_until_list : sm_formula COLON unless_until_event COLON RESUME ID SEMI
		          | sm_formula COLON unless_until_event COLON RESTART ID SEMI
		          | unless_until_list sm_formula COLON unless_until_event COLON RESUME ID SEMI
		          | unless_until_list sm_formula COLON unless_until_event COLON RESTART ID SEMI
	unless_until_event : empty
			   | ID'''
			

def p_onentry_onexit(p):
	'''onentry : ONENTRY LBRACE sm_assignment_list RBRACE
		   | ONENTRY sm_assignment_list
	onexit : ONEXIT LBRACE sm_assignment_list RBRACE
	       | ONEXIT sm_assignment_list'''


def p_sm_assignment(p):
	'''sm_assignment : ID EQUALS sm_formula
	sm_assignment2 : sm_assignment SEMI
		       | ID EQUALS sm_assignment SEMI
	sm_assignment_list : sm_assignment2
			   | sm_assignment_list sm_assignment2'''
	

def p_state_attrib(p):
	'''state_attrib : empty
			| INITIAL
			| FINAL
			| INITIAL FINAL
			| FINAL INITIAL'''


def p_sm_formula(p):
	'''sm_formula : ID
		      | TRUE
		      | FALSE
		      | NEG sm_formula 
		      | LPAREN sm_formula RPAREN
		      | PRE sm_formula 
		      | LAST sm_formula 
		      | sm_formula MINUSGT sm_formula 
		      | sm_formula OOR sm_formula 
		      | sm_formula AAND sm_formula''' 
	

def p_sm_if(p):
	'''sm_if : IF LPAREN sm_formula RPAREN LBRACE state_body RBRACE sm_elif
	         | IF LPAREN sm_formula RPAREN state_body sm_elif
	sm_elif : sm_else
                | ELIF LPAREN sm_formula RPAREN LBRACE state_body RBRACE sm_elif
	        | ELIF LPAREN sm_formula RPAREN state_body sm_elif
	sm_else : empty
		| ELSE LBRACE state_body RBRACE
		| ELSE state_body'''


def p_speclet(p):
	'''speclet : td
		   | qddc'''


def p_speclet_td(p):
	'''td : TD FUNCTIONID arg LBRACE timing_diagram_wf sync RBRACE
	      | TD FUNCTIONID arg LBRACE NOMINAL nominal_vars SEMI timing_diagram_wf sync RBRACE
	      | TD FUNCTIONID arg LBRACE timing_diagram_wf RBRACE
	      | TD FUNCTIONID arg LBRACE NOMINAL nominal_vars SEMI timing_diagram_wf RBRACE
	      | TD FUNCTIONID arg ID COLON waveform SEMI
	      | TD FUNCTIONID arg ID COLON waveform STUTTER SEMI'''


def p_arg(p):
	'''arg : LPAREN arg_list RPAREN
	       | LPAREN RPAREN	
	arg_list : ID
	         | XTDVAL
	         | arg_list COMMA ID
	         | arg_list COMMA XTDVAL'''

		
def p_timing_diagram_wf(p):
	'''timing_diagram_wf : null_non_null COLON waveform SEMI 
			     | timing_diagram_wf null_non_null COLON waveform SEMI
	null_non_null : ID
		      | ATNULL'''


def p_waveform(p):
	'''waveform : nominal_or_empty waveform_atomic
		    | waveform nominal_or_empty waveform_atomic
	nominal_or_empty : empty
			 | nominal
	waveform_atomic : td_val
		        | td_val STUTTER
		        | LBRACKET number LBRACKET td_val DRBRACKET'''


def p_td_val(p):
	'''td_val : TRISTATE
		  | TDVAL
		  | XTDVAL'''


def p_sync(p):
	'''sync : ATSYNC COLON edge_list SEMI
	edge_list : edge
		  | edge_list COMMA edge
	     edge : LPAREN ID COMMA ID COMMA expression RPAREN
		  | LPAREN ID COMMA ID COMMA LPAREN expression COMMA RPAREN RPAREN
		  | LPAREN ID COMMA ID COMMA LPAREN COMMA expression RPAREN RPAREN
		  | LPAREN ID COMMA ID COMMA LPAREN expression COMMA expression RPAREN RPAREN
		  | LPAREN ID COMMA ID COMMA LBRACKET expression COMMA RPAREN RPAREN
		  | LPAREN ID COMMA ID COMMA LBRACKET COMMA expression RPAREN RPAREN
		  | LPAREN ID COMMA ID COMMA LBRACKET expression COMMA expression RPAREN RPAREN
		  | LPAREN ID COMMA ID COMMA LPAREN expression COMMA RBRACKET RPAREN
		  | LPAREN ID COMMA ID COMMA LPAREN COMMA expression RBRACKET RPAREN
		  | LPAREN ID COMMA ID COMMA LPAREN expression COMMA expression RBRACKET RPAREN
		  | LPAREN ID COMMA ID COMMA LBRACKET expression COMMA RBRACKET RPAREN
		  | LPAREN ID COMMA ID COMMA LBRACKET COMMA expression RBRACKET RPAREN
		  | LPAREN ID COMMA ID COMMA LBRACKET expression COMMA expression RBRACKET RPAREN'''

		
def p_number(p):
	'''number : TRISTATE
		  | NUMBER'''


def p_nominal(p):
	'''nominal : LT ID GT'''


def p_qddc(p):
	'''qddc : QDDC FUNCTIONID arg LBRACE NOMINAL nominal_vars SEMI with_statements RBRACE
		| QDDC FUNCTIONID arg with_statements 
		| QDDC FUNCTIONID arg LBRACE with_statements RBRACE'''
	

def p_nominal_vars(p):
	'''nominal_vars : ID
			| nominal_vars COMMA ID'''


#def p_arithematic_bop(p):
#	'''arithematic_bop : PLUS
#			   | MINUS
#			   | TIMES
#			   | DIVIDE
#			   | MOD'''


def p_logical_bop(p):
	'''logical_bop : OOR
		       | AAND
	  logical_bop2 : logical_bop
		       | IMPLICATION	
		       | IFF	
	dc_bop : logical_bop2
	       | CARET'''	


def p_multi_assign(p):
	'''multi_assign : empty
  		        | multi_assign_ne
	multi_assign_ne : assignment
			| multi_assign_ne COMMA assignment
			| LPAREN multi_assign_ne RPAREN'''

	
def p_assignment(p):
    	'''assignment : id EQUALS expression
                      | id PLUSPLUS
                      | PLUSPLUS id
                      | id MINUSMINUS
                      | MINUSMINUS id
                      | id c_op id_exp_num
                      | LPAREN assignment RPAREN
	c_op : PLUSEQUALS
		| MINUSEQUALS
		| TIMESEQUALS
		| DIVEQUALS
		| MODEQUALS
		| RAISEEQUALS''' 


def p_rel_exp(p):
    	'''rel_exp : expression rel_op expression
                   | LPAREN rel_exp RPAREN'''


def p_expression(p):
	'''expression : id
	              | number
	              | TDVAL
	              | XTDVAL
	              | expression PLUS expression
                      | expression MINUS expression
                      | expression TIMES expression
                      | expression DIVIDE expression
                      | expression MOD expression
                      | expression RAISE expression
		      | MINUS expression	
		      | LPAREN expression RPAREN'''


def p_rel_op(p):
	'''rel_op : EQ
		  | NE
		  | LT
		  | LE
		  | GT
		  | GE
	dc_rel_op : EQ
		  | LT
		  | LE
		  | GT
		  | GE'''


def p_bitwise_exp(p):
	'''bitwise_exp : id AND id
		       | id STUTTER id	
		       | id CARET id	
		       | id DLT id	
		       | id DGT id	
		       | TILDE id'''
	
			 
def p_logical_exp(p):
    	'''logical_exp : expression
		       | rel_exp
		       | bitwise_exp
		       | logical_exp logical_bop rel_exp
                       | NEG logical_exp
                       | LPAREN logical_exp RPAREN'''


def p_printf(p):
	'''printf : PRINTF LPAREN SCONST RPAREN SEMI
		  | PRINTF LPAREN SCONST printf_list RPAREN SEMI
	printf_list : COMMA logical_exp
		    | printf_list COMMA logical_exp'''


#def p_expression_name(p):
#    "expression : ID"
#    try:
#        p[0] = names[p[1]]
#    except LookupError:
#        print("Undefined name '%s'" % p[1])
#        p[0] = 0
#

def p_error(p):
	print "def p_error(p):",  type(p), p.type, p.lexpos, lineD[prev_key(p.lexpos)]
	print "here", lineD[p.lexpos]["lineno"]
    	if p:
#		print "is_invalid:"
	      	if is_invalid(p):
			if called_by_gui:
				with open("error.log", "w") as fhw:
					fhw.write(str(lineD[p.lexpos]["lineno"]))
				exit(1)
			else:
				removed_newline = 0
				error_line_no = int(lineD[p.lexpos]["lineno"])
				with open("error.map", "r") as fh:
					L = list()
					for line in fh:
						L.append(int(line))
					j = 0
					while((j < len(L)) and (L[j]-removed_newline <= error_line_no)):
						removed_newline += L[j+1]
						j += 2
		        	print("\x1b[1;31;40mLine %s: syntax error\x1b[0m\n" % str(error_line_no+removed_newline))
#				raise Exception(lineD[p.lexpos]["lineno"])
				i = 0
#				lexpos = prev_key(p.lexpos) if prev_key(p.lexpos) else p.lexpos
				for line in code.splitlines(True):
					if(i <= p.lexpos <= i+len(line)):
						print "\t"+line.rstrip("\n")
						line = re.sub(r'[^\s]', r' ', line[:p.lexpos-i])
						print "\t"+line+"^"		
						exit(1)	
					else:
						i += len(line)		
    	else:
		with open("error.log", "w") as fhw:
			fhw.write("1")
		exit(1)
#        	print("Syntax error at EOF")


def is_invalid(p):
	print "def is_invalid(p):", p.type
        if p.type != "SEMI":
		return True
	elif not prev_key(p.lexpos):
		return True
	elif lineD[prev_key(p.lexpos)]["token"] == "SEMI":
		return True
	elif lineD[prev_key(p.lexpos)]["token"] in ["LT", "LE", "GT", "GE", "NE"] and lineD[prev_key(prev_key(p.lexpos))]["token"] in ["RPAREN", "ID"]:
		return True
	else:
		if is_in_function(p):
#			print "yes in function"
			if (lineD[prev_key(p.lexpos)]["token"] != "COMMA"):
				if (lineD[prev_key(p.lexpos)]["token"] != "LPAREN") or (lineD[prev_key(prev_key(p.lexpos))]["token"] != "FUNCTIONID"):
					return True
		else:
			if is_in_encode(p):
				return True
			else:
				i = is_in_loop(p)
#				print "i=", i
				if i:
					return True if i in [1, 2, 3] else False
				elif not_qddc(p):
					return True
		return False
	

def prev_key(key):
	L = sorted(lineD.keys())
#	print "def prev_key(key):", L
	if L[0] >= key:
		return None
	for i in range(1, len(L)):
		if L[i] >= key:
			return L[i-1]
	return L[len(L)-1]


def is_in_function(p):
#	print "def is_function(p):"
	L = sorted(lineD.keys())
	L2 = list()
#	print "L=", L, p.lexpos, prev_key(p.lexpos)
	n = L.index(prev_key(p.lexpos))
#	print "n = ", n, L[n]
#	paren_depth = 0
#	paren_balanced = True
	for i in range(n, 0, -1):
#		print "here", L[i], lineD[L[i]]["token"]
		if lineD[L[i]]["token"] in ["LBRACE", "RBRACE", "SEMI"]:
			return False
		elif (lineD[L[i-1]]["token"] == "FUNCTIONID") and (lineD[L[i]]["token"] == "LPAREN") or (lineD[L[i]]["token"] == "COMMA"):
#			print "paren_deth=", paren_depth
			return in_function(L2)
		elif lineD[L[i]]["token"] in ["LPAREN", "RPAREN"]:
			L2.insert(0, lineD[L[i]]["token"])
#		elif lineD[L[i]]["token"] not in valid_tokens:
#			return False
#		elif (lineD[L[i]]["token"] not in ["ID", "RBRACKET"]) and (lineD[L[i+1]]["token"] == "LBRACKET"):
#			return False
	return False   


def in_function(L):
#	print "def in_function(L):", L
	paren_depth = 0
#	paren_balanced = True
	for i in range(len(L)):
#		print "here", L[i], lineD[L[i]]["token"]
#		if not paren_balanced:
#			return True
		if L[i] == "RPAREN":
			if paren_depth == 0:
				return False
			else:
				paren_depth -= 1
#				paren_balanced = False if paren_balanced and (paren_depth < 0) else paren_balanced
		else:
			paren_depth += 1
	return True   
		

def is_in_loop(p):
#	print "def in_loop(p):"
#	print sorted(lineD.keys())
	L = [("FOR", 2), ("WITH", 6), ("WITHPAR", 6)] 
	for lex, n in L:
#		print "*******lex=", lex
		i = 0;
		key = prev_key(p.lexpos)
		while((i <= n) and key):
#			print "here", lineD[key]["token"]
			if lineD[key]["token"] == lex:
				return i+1
			elif lineD[key]["token"] in ["LBRACE", "RBRACE"]:
				break
			elif lineD[key]["token"] == "SEMI":
				i += 1
			key = prev_key(key)
	return False


def is_in_encode(p):
#	print "def is_in_encode(p):"
	L = ['HINCLUDE', 'HDEFINE', 'HDCLIB', 'INFERG', 'INFERF', 'INFERGF', 'INFERFG', 'CONST', 'INTERFACE', 
	'IMPLIES', 'INIT', 'ANTI', 'TRIGGERS', 'FOLLOWS', 'PREF', 'PPREF', 'DC', 
	'HIMPLIES', 'HINIT', 'HANTI', 'HTRIGGERS', 'HFOLLOWS', 'HPREF', 'HPPREF', 'HDC', 'NODE']
	key = prev_key(p.lexpos)
	while(key):
#		print lineD[key]["token"]
		if lineD[key]["token"] == "ENCODE":
			return True
		elif lineD[key]["token"] in L:
			return False
		key = prev_key(key)
	return False
	

def not_qddc(p):
#	print "def not_qddc(p):"
	key = prev_key(p.lexpos)
#	print lineD[key]["token"]
	while(key and lineD[key]["token"] in valid_qddc_tokens+["DGT", "COLON"]):
		key = prev_key(key) 
	if lineD[key]["token"] == "SOFTREQ":
		return False
	else:
		while(key and lineD[key]["token"] in valid_qddc_tokens):
			key = prev_key(key) 
#			print lineD[key]["token"]
		return False if key and lineD[key]["token"] in ["LBRACE", "RBRACE", "SEMI"] else True
	

def parse(fName, disableParsing):
	global code
	with open(fName, "r") as fh:
		code = ""
		for line in fh:
			i = 0;
			while(i < len(line)):
				if re.match(r'{{', line[i:]):
					s = re.match(r'{{([ \t]*[A-Za-z(!;])', line[i:])
					if s:
						i += len(s.group(0))
					else:
						line = line[:i] + re.sub(r'{{', r'{ { ', line[i:], count=1)
						i += 4
				elif re.match(r'}}', line[i:]):
					s = re.match(r'}}([ \t]*[<=&|;^)])', line[i:])
					if s:
						i += len(s.group(0))
					else:
						line = line[:i] + re.sub(r'}}', r' } }', line[i:], count=1)
				else:
					i += 1
			code += line
	with open("error.map", "w") as fh:
		i = 0
		j = 0
		while(i < len(code)):
			if code[i] == "\n":
				j += 1
			else:
				s = re.match(r'/\*(.|\n)*?\*/', code[i:])
				if s:
					fh.write(str(j)+"\n"+str(s.group(0).count("\n"))+"\n")
#					print str(s.group(0)), "*"+code[i+len(s.group(0))]+"*"
					j += s.group(0).count("\n")
					i += len(s.group(0))-1 
			i += 1
	code = re.sub(r'/\*(.|\n)*?\*/', r'', re.sub(r'//.*?(?=\n)', r'', code))
	code = re.sub(r'([A-Za-z][A-Za-z0-9_]*)((\[[A-Za-z_][A-Za-z0-9_]*\])+)', r'\1\2 ', code)
	gLexer.input(code)
	for tok in gLexer:
		lineD[tok.lexpos] = dict({"lineno": tok.lineno, "token": tok.type})
		print tok
#	print code
	if not disableParsing:
#	        try:
	        yacc.parse(input=code, lexer=gLexer, debug=False)
#		except Exception as inst:
#		        raise Exception(inst[0]) 
	code = re.sub(r'([A-Za-z][A-Za-z0-9_]*)((\[[A-Za-z_][A-Za-z0-9_]*\])+) ', r'\1\2', code)
	return code





##     __main()__



def main(fName, disableParsing, caller_gui):
#	print "def main(fName, disableParsing):", fName
	global gLexer, lineD, code, called_by_gui
	called_by_gui = caller_gui
	gLexer = qsflex.lexer
	lineD = dict()
	yacc.yacc()
	allCode = ""
	return parse(fName, disableParsing)
