rm *.dfa *.tra *.lab *.rew *.scade *.lus *.soft* *.hard* *.ec stats.txt
qsf G9e8.qsf --disable-parsing
synth2 G9e8.hardreq.dfa G9e8.softreq.dfa G9e8.io foo scadeBounded 1 1 synth.config > G9e8.scade
synth_deterministic IndicatorsProjectedOut.dfa G9e8.det.io foo 0 > G9e8.det.scade

