#!/bin/python

import xml.etree.ElementTree as ET
import re, process_dc


def unwind_wave(wf):
#	print "def unwind_wave(wf):", wf
	wf2 = ""
	L = re.findall(r'(\[\d+\[[0|1|2|x]+\]\]|<[A-Za-z][A-Za-z0-9_]*>|(?<!\[)\d+|x+|\|)', wf)
#	print "L=", L
	for l in L:
		if re.match(r'\[\d+\[[0|1|2|d|x]+\]\]', l):
			L1 = re.findall(r'(\d+|x)', l)
			wf2 += int(L1[0])*L1[1]

		else:
#			s = re.findall(r'^(<[A-Za-z][A-Za-z0-9_]*>)', l)
#			if s and (s[0] not in nodeL):
#				nodeL.extend(s)
#				print "nodeL=", nodeL
			wf2 += l
#	print "wf2=", wf2, wf
	return wf2

def replace_repetition_with_dots(wf):
#	print "def replace_repetition_with_dots(wf):", wf
	c2 = ""
	wf1 = ""
	for i in range(len(wf)):
		wf1 += "." if (wf[i] == c2) and ((i == len(wf)-1) or (wf[i+1] != "|")) else wf[i]
		if wf[i] in ["0", "1", "2", "x", "|", "<", "["]:
			c2 = wf[i]
#	print "wf=", wf, "wf1=", wf1
	return wf1

#def sync_wf():
#	sync = True
#	while(sync):
#		sync = False
#		for node in nodeL:
#			maxIndex = max([wf.index(node) for wfName, wf in wfL if node in wf])
#			for i in range(len(wfL)):
#				if (node in wfL[i][1]) and (wfL[i][1].index(node) < maxIndex):
#					if update_wf(i, node, maxIndex - wfL[i][1].index(node)):
#						sync = True


def translate_waveform(wfName, wf):
#	print "in build_formula(node):"+wfName+"*"
	i = 0
	wfNodeL = list()
	while(i < len(wf)):
		s = re.match(r'(<[A-Za-z0-9_]+>(0|1|2|x))', wf[i:])
		if s:
			wfNodeL.append(wf[i:i+len(s.group(0))-1]) 
			i += len(s.group(0))
		else:
			wfNodeL.append("")
			i += 1
	wf = re.sub(r'<[A-Za-z0-9_]*>', "", wf)
#	print "wfNodeL=", wfNodeL, wf
	
	wfTrans = ""
	slen = 0
	for i in range(len(wf)):
		if wf[i] in ["0", "1", "2", "x"]:
			slen = 1 
			if i > 0:
				wfTrans += " ^ "
			wfTrans += (wfNodeL[i]+" ^ ") if wfNodeL[i] else ""
			if (i+1 == len(wf)-1) and (wf[i+1] == "|"):
				if wf[i] == "0":
					wfTrans += "(pt || [!("+wfName+")])"
				elif wf[i] == "1":
					wfTrans += "(pt || ["+wfName+"])"
				elif wf[i] == "2":
					wfTrans += "true"
				elif wf[i] == "x":
					wfTrans += "(pt || [!("+wfName+")] || ["+wfName+"])"
			else:
				if wf[i] == "2":
					if i == len(wf)-1:
						wfTrans += "(slen == 1)"
					else:
						if wf[i+1] == ".":
							wfTrans += "(ext"
						elif wf[i+1] in ["0", "1", "2", "x"]:
							wfTrans += "(slen == 1)"
						else: 
							wfTrans += "true" 
				elif wf[i] in ["0", "1"]:
					s1 = "!("+wfName+")" if wf[i] == "0" else wfName
					if i == len(wf)-1:
						wfTrans += "{{"+s1+"}}"
					elif wf[i+1] == ".":
						wfTrans += "(["+s1+"]"
					elif wf[i+1] == "|":
						wfTrans += "(pt || ["+s1+"])"
					else:
						wfTrans += "{{"+s1+"}}"
				else:
					if i == len(wf)-1:
						wfTrans += "pt"
					elif wf[i+1] == ".":
						wfTrans += "(([!("+wfName+")] || ["+wfName+"])"
					elif wf[i+1] == "|":
						wfTrans += "(pt || [!("+wfName+")] || ["+wfName+"])"
					else:
						wfTrans += "(slen == 1)"
		elif wf[i] == ".":
			slen += 1
			if (i < len(wf)-1) and (wf[i+1] != ".") or (i == len(wf)-1):
				wfTrans += " && (slen == "+str(slen)+"))"
#	print wf
#	print wfTrans
	return wfTrans, map(lambda x: x[1:-1], filter(lambda x: x, wfNodeL))


def get_speclet_param(wfName, wfNodeL):
#	print "def get_speclet_param(wfName=None, wf=None, wfNode=None):", wfName, wfNodeL
	wfParam = wfName+", " if wfName != "@null" else ""
	for s in wfNodeL:
#		if s:
		wfParam += s+", "
#	print "wfParam*"+wfParam[:-2]+"*"
	return wfParam[:-2]

	
def timing_relations(edges, F, Hconstant):
#	print "def timing_relations():", edgeL
	parenDepth = -1
	for i in range(len(edges)):
		if (i > 0) and (edges[i-1] != "\\") and (edges[i] == "(") or (i == 0) and (edges[i] == "(") or (edges[i] == "["):
			parenDepth += 1 if parenDepth > 0 else 2
		elif (i > 0) and (edges[i-1] != "\\") and (edges[i] == ")") or (i == 0) and (edges[i] == ")") or (edges[i] == "]"):
			parenDepth -= 1 
			if parenDepth == 0:
				parenDepth = -1
				edges = edges[:i]+";"+edges[i+1:]
	for edge in edges.split(";")[:-1]:
		edgeL.append(re.sub(r'^[ ]*[,]?[ ]*\(', r'', edge))
	
	L = list()
	L1 = list()
	wfParam = ""
	for edge in edgeL:
#		print "edge=", edge
#		if len(re.split(r',', edge)) == 3:
		L.append(re.split(r',', edge))
#		print "edge=", edge, "L[-1]=", L[-1][0], L[-1][1] 
		wfParam += L[-1][0].strip()+", " if L[-1][0].strip() not in wfParam else ""
		wfParam += L[-1][1].strip()+", " if L[-1][1].strip() not in wfParam else ""
		L1.extend(filter(lambda x: (len(x)) and (not x.isdigit()) and (x not in L1) and (x not in Hconstant), re.findall(r'([A-Za-z0-9]*)', L[-1][2])))
		if len(L[-1]) == 4:
			L1.extend(filter(lambda x: (len(x)) and (not x.isdigit()) and (x not in L1) and (x not in Hconstant), re.findall(r'([A-Za-z0-9]*)', L[-1][3])))
	wfParam = reduce(lambda x, y: x+y, [z+", " for z in L1], "")+wfParam
	if len(L) > 0:
		wfTiming = ""
		for i in range(len(L)):
			wfTiming += "(true ^ <"+L[i][0].strip()+"> ^ ("
			if len(L[i]) == 4:
				if L[i][2].strip() not in ["[", "("]:
					wfTiming += "(slen >"+("= " if L[i][2].strip()[0] == "[" else " ")+str(process_dc.eval_expr(L[i][2].strip()[1:], F))+")"
				if L[i][2].strip() not in ["[", "("] and L[i][3].strip() not in ["]", ")"]:
					wfTiming += " && "
				if L[i][3].strip() not in ["]", ")"]:
					wfTiming += "(slen <"+("= " if L[i][3].strip()[-1] == "]" else " ")+str(process_dc.eval_expr(L[i][3].strip()[:-1], F))+")"
			else:
					wfTiming += "(slen == "+str(process_dc.eval_expr(L[i][2].strip(), F))+")"
			wfTiming += ") ^ <"+L[i][1].strip()+"> ^ true)" +(" && " if i+1 < len(L) else "")
#			else:
#				fhw.write("(true ^ <"+L[i][0].strip()+"> ^ <"+L[i][1].strip()+"> ^ true)")
				
#			if i+1 == len(L):
#				fhw.write("\n\n" if macro == "macro" else ";\n\n")
#			else:
#				fhw.write(" && ")
	return wfTiming, wfParam.rstrip(", ")

#def node_end_points(node):
#	for wfName, wf, wfNode in wfL:
#		index = wfNode.find(node)
#		if index == 0 :
#			return "("
#		elif index > 0:
#			return "(true ^ " 

#def get_node_name():
#	valRange=[["03B0", "03CE"], ["05D0", "05E0"], ["0904", "0939"], ["0C85", "0C8C"], ["0C8E", "0C91"], ["0C93", "0CA8"], 
#		["0CB5", "0CB9"], ["0D05", "0D0C"], ["0D0E", "0D1A"], ["0D60", "0D61"], ["0D66", "0D75"]]
#	for i, j in valRange:
#		for k in range(int(i, 16), int(j, 16)):
#			nodeNames.append("\u"+hex(k).replace("x", "")) 



#######          __main__


def main(root, specletId, param, F, Hconstant):
#	print "def main(fhw, root, specletId, paramL):", specletId, param, Hconstant
	global edgeL, nominalL
	wf = list()
	edgeL = list()
	nominalL = list()
	for child in root:
#		print "child.tag=", child.tag #attrib["data"]
		if child.tag == "nominal":
			child.attrib["id"] = "nominal"
		else:
			L = child.attrib["data"].split(":")
#			print "L=", L
			if re.match(r'@sync', L[0].strip()):
				child.D["trans"], wfParam = timing_relations(L[1], F, Hconstant)
				child.attrib["id"] = specletId+"timingrelation"
				child.attrib["paramspeclet"] = "("+wfParam+")"
#				print child.D["id"], child.D["paramspeclet"]
			else:
				wf.append([L[0], L[1]])
				child.D["trans"], child.D["nominals"] = translate_waveform(L[0].strip(), replace_repetition_with_dots(unwind_wave(L[1].strip())))
				nominalL.extend(filter(lambda x: x not in nominalL, child.D["nominals"]))
				child.attrib["id"] = specletId+"waveform"+(L[0].strip() if L[0].strip() != "@null" else "")
				child.attrib["paramspeclet"] = "("+get_speclet_param(L[0].strip(), child.D["nominals"])+")"
#				print child.D["id"], child.D["paramspeclet"]
#	print "param=", param[1:-1]
#	print "nominalL=", nominalL, "("+(reduce(lambda x, y: x+y+", ", [param[1:-1]]+nominalL if param[1:-1] else nominalL, ""))[:-2]+")"
	if root.children[0].tag == "nominal":
		nominalL = root.children[0].attrib["data"]
	root.attrib["paramspeclet"] = "("+(reduce(lambda x, y: x+y+", ", [param[1:-1]]+nominalL if param[1:-1] else nominalL, ""))[:-2]+")"
#	print root.attrib["paramspeclet"]
#	print specletId+root.attrib["paramspeclet"], nominalL if root.children[0].tag != "nominal" else root.children[0].attrib["data"]
	return specletId+root.attrib["paramspeclet"], nominalL, wf
#	return specletId+root.attrib["paramspeclet"], nominalL
	

