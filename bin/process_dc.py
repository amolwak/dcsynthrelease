#!/bin/python

import re
import build_tree

def eval_expr(expr, F):
#	print "def eval_expr(expr, F):", expr, F
	for key in F:
		exec("%s=%d"%(key, F[key]))
	try:
		return eval(expr)
	except:	
		return expr

def get_var(text, F):
#	print "def get_var(text, F):*"+text+"*", F
	L = get_exp_list(text)
#	print "L=", L #,"*"+text2+"*"
#	print "L=", reduce(lambda x, y: x+y, L, "")
	for key in F:
		exec("%s=%d"%(key, F[key]))
#	print "L=", L #,"*"+text2+"*"
	for __I__ in range(1, len(L)):
#		print L[__I__], type(L[__I__])
#		if L[__I__] != "[]":
		try:
#			print "here=", L[__I__],
#			L[__I__] = eval(L[__I__])
			eval(L[__I__])
#			print L[__I__-1], L[__I__], "*"+L[__I__+1]+"*"
#			print L[__I__]
			L[__I__-1] = "@" if L[__I__-1].strip() == "[" else L[__I__-1]
			L[__I__+1] = "@" if L[__I__+1].strip() == "]" else L[__I__+1]
		except:
			pass
#	print "L2=", L
	for s in [L[__I__] for __I__ in range(len(L)) if L[__I__] is not "@"][1:]:
#		print "s=", s
		try:
			L[0] += str(eval(s))+", "
		except:
#			print "*"+s3+"*", s2		
			L[0] += s+", "		
		L[0] = L[0][:-2]
#	print ", ".join([z.strip() for z in str(L[0]).split(",")])
	return ", ".join([z.strip() for z in str(L[0]).split(",")])
#	return str(L[0])


#def get_var(text, F):
##	print "def get_var(text, F):*"+text+"*", F
#	L = get_exp_list(text)
##	print "L=", L #,"*"+text2+"*"
##	print "L=", reduce(lambda x, y: x+y, L, "")
#	for key in F:
#		exec("%s=%d"%(key, F[key]))
##	print "L=", L #,"*"+text2+"*"
#	for __I__ in range(1, len(L)):
##		print L[__I__], type(L[__I__])
##		if L[__I__] != "[]":
#		try:
##			print "here=", L[__I__],
#			L[__I__] = eval(L[__I__])
##			eval(L[__I__])
##			print L[__I__-1], L[__I__], "*"+L[__I__+1]+"*"
##			print L[__I__]
#			L[__I__-1] = "@" if L[__I__-1].strip() == "[" else L[__I__-1]
#			L[__I__+1] = "@" if L[__I__+1].strip() == "]" else L[__I__+1]
#		except:
#			pass
#	print "L2=", L
#	for s in [L[__I__] for __I__ in range(len(L)) if L[__I__] is not "@"][1:]:
#		print "s=", s
#		for s2 in re.split(r'(?<=\()([A-Za-z0-9_+-/*%(), ]+)(?=\))', str(s)):
##			print "s2="+str(s2)+"*"
#			for s3 in re.split(r',', s2):
##				print "s3=", s3
#				try:
#					L[0] += str(eval(s3))+", "
#					print "eval=", str(eval(s3))
#				except:
##					print "*"+s3+"*", s2		
#					L[0] += s3+", "		
#			L[0] = L[0][:-2]
#	print ", ".join([z.strip() for z in str(L[0]).split(",")])
#	return ", ".join([z.strip() for z in str(L[0]).split(",")])
##	return str(L[0])


def get_exp_list(text):
#	print "def get_exp_list(text):", text
	L = list()
	L2 = list()
	for s in re.split(r'([A-Za-z][A-Za-z0-9]*)', text):
		if not re.match(r'([A-Za-z][A-Za-z0-9]*)', s):
			for s2 in re.split(r'([0-9]+)', s):
				for s3 in re.split(r'([()])', s2):
					for s4 in re.split(r'([\[\]])', s3):
						L.append(s4)
		else:
			L.append(s)
#	L = filter(lambda x: x, [z.strip() for z in L])+["$"]
#	print "L2=", L, text
	L = crop_list(L)+["$"]
#	print "L=", L
#	L = filter(lambda x: ' ' if re.match(r'\s+', x) else x.strip(), L)+["$"]
	s = L[0]
	for i in range(1, len(L)):
#		print L[i],
		j = i-2
		while((j >= 0) and (not L[j])):
			j -= 1
		s2 = re.match(r'[A-Za-z][A-Za-z0-9_]*', L[j].strip()) if j >=0 else None
		if (not s2) and re.match(r'\(', L[i-1]) and (re.match(r'([A-Za-z][A-Za-z0-9]*)', L[i]) or re.match(r'([0-9]+)', L[i])):
			s += L[i] 		
		elif re.match(r'[+\-\*/%]', L[i-1]) and (re.match(r'([A-Za-z][A-Za-z0-9]*)', L[i]) or re.match(r'([0-9]+)', L[i])):
			s += L[i] 		
		elif (re.match(r'([A-Za-z][A-Za-z0-9]*)', L[i-1]) or re.match(r'([0-9]+)', L[i-1]) or re.match(r'\)', L[i-1])) and re.match(r'[+\-\*/%]', L[i]):
			s += L[i]
		elif (re.match(r'([A-Za-z][A-Za-z0-9]*)', L[i-1]) or re.match(r'([0-9]+)', L[i-1]) or re.match(r'\)', L[i-1])) and re.match(r'\)', L[i]) and (s.count("(") >= s.count(")")+1):
			s += L[i]
		elif (re.match(r'[+\-\*/%]', L[i-1])) and (re.match(r'\(', L[i]) or re.match(r'([A-Za-z][A-Za-z0-9]*)', L[i]) or re.match(r'([0-9]+)', L[i])):
			s += L[i]
#		elif L[i-1] == "," or L[i] == ",":
#			s += L[i]
#		elif L[i] == ",":
#			s += L[i]
		else:
			L2.append(s)
#			L2.append(L[i])
#			print "s=", s
			s = L[i]
				
#	print "here L2=", L2
	return L2


def crop_list(L):
#	print "def crop_list(L):", L
	for i in range(len(L)):
		if L[i].isspace():
			L[i] = ' ';
		else:
			L2 = re.split(r'([^\s]+)', L[i])
			L[i] = ""
			for s in L2:
				L[i] += ' ' if s.isspace() else s
			
#	print "new L=", reduce(lambda x, y: x+y, L, "")
	return filter(lambda x: x, L)
		
		
