#!/usr/bin/python3.3

import xml.etree.ElementTree as ET
import sys, copy, Queue, re


def build_formula(fhw, root, cur_inst, dictNom, dictTimer, listInvar, isLast, params=False):
#	print "def build_formula(root):", root.tag, isLast, dictTimer
	if root.tag == "chart":
		argsInstL = list()
		for inst in root:
			paramL, nomL = get_inst_param(inst, list(), list())
			params = "["+reduce(lambda x, y: x+y+", ", [z for z in paramL+nomL], "")[:-2]+"]"
			argsInstL.append((inst.attrib["id"], paramL, params))
			build_formula(fhw, inst, inst, dictNom, dict(), list(), False, params)
		paramL1 = list()
		for z1, z2, z3 in argsInstL:
			paramL1 += [z for z in z2 if z not in paramL1]
		paramL2 = "["+reduce(lambda x, y: x+y+", ", paramL1+[key for key in dictNom], "")[:-2]+"]"
		fhw.write("define "+root.attrib["id"]+paramL2+" as\n\t(")
#		if dictNom:
#			scount = reduce(lambda x, y: x+"ex "+y+". ", [key for key in dictNom], "")
#			scount += "("+reduce(lambda x, y: x+"(scount "+y[0]+" = "+str(y[1])+") && ", [(key, dictNom[key]) for key in dictNom], "")
#			fhw.write(scount)
		formula = reduce(lambda x, y: x+y+" && ", [z1+z3 for z1, z2, z3 in argsInstL], "")[:-4] 
		fhw.write(formula+");\n\n")
		return (root.attrib["id"]+paramL2, paramL1, [(key, dictNom[key]) for key in dictNom])
	elif root.tag == "instance":
		fhw.write("define "+root.attrib["id"]+"events"+params+" as\n\t(")
		for i in range(len(root._children)-1):
			isLast = all(node.tag == "invariant" for node in root._children[i+1:])
			build_formula(fhw, root._children[i], root, dictNom, dictTimer, listInvar, isLast)
		build_formula(fhw, root._children[len(root._children)-1], root, dictNom, dictTimer, listInvar, not isLast)
		fhw.write(" ^ true);\n\n")
		formulaInst = [root.attrib["id"]+"events"+params]
		if dictTimer:
			paramTimers = "["+reduce(lambda x, y: x+", "+y, [key for key in dictTimer])+"]"
			fhw.write("define "+root.attrib["id"]+"timingrelation"+paramTimers+" as\n\t(")
			formula = ""
			for key in dictTimer:
				n = 0
				formula += "(("
				for z in dictTimer[key]:
					n += int(z)
					formula += "([!"+key+"] ^ ["+key+" && (slen = "+z+")] || ["+key+" && (slen = "+z+"])) ^ "
				formula += "[[!"+key+"]]) && (scount n = "+str(n)+")) && " 
			fhw.write(formula[:-4]+");\n\n")
			formulaInst.append(root.attrib["id"]+"timingrelation"+paramTimers)
		if listInvar:
			paramInvars = list()
			for z1, z2, z3 in listInvar:
				if z1 not in paramInvars:
					paramInvars.append(z1)
				if z2 not in paramInvars:
					paramInvars.append(z2)
			paramInvars = "["+reduce(lambda x, y: x+", "+y, [z for z in paramInvars])+"]"
			formula = reduce(lambda x, y: x+" && "+y, [z3 for z1, z2, z3 in listInvar]) 
			fhw.write("define "+root.attrib["id"]+"invariant"+paramInvars+" as\n\t("+formula+");\n\n")
			formulaInst.append(root.attrib["id"]+"invariant"+paramInvars)
		fhw.write("define "+root.attrib["id"]+params+" as\n\t"+reduce(lambda x, y: x+" && "+y, formulaInst)+";\n\n")
	elif root.tag == "loop":
		for i in range(int(root.attrib["repeat"])-1):
			build_formula(fhw, root._children[i], dictNom, dictTimer, False)
		build_formula(fhw, root._children[len(root._children)-1], cur_inst, dictNom, dictTimer, listInvar, True and isLast, paramL)
	elif root.tag == "simregion":
		sim_region(fhw, root, cur_inst, dictNom, dictTimer, listInvar, isLast)
	elif root.tag == "coregion":
		coregion(fhw, root, cur_inst, dictNom, dictTimer, listInvar, isLast)
	elif root.tag == "timer":
		update_dicttimer(dictTimer, root)
		fhw.write("true ^ {{"+root.text.strip()+"}} ^ ")
	elif root.tag == "timeout":
		if isLast and isLastEvent(cur_inst, root):
			fhw.write("pt")
	elif root.tag in ["transmit", "ack", "condition"]:
		update_dictnominals(dictNom, root)
		if isLast and isLastEvent(cur_inst, root):
			fhw.write("true ^ (<"+root.attrib["id"]+"> && <"+root.text.strip()+">)")
		else:
			fhw.write("true ^ (<"+root.attrib["id"]+"> && {{"+root.text.strip()+"}}) ^ ")
	elif root.tag == "action":
		if isLast and isLastEvent(cur_inst, root):
			fhw.write("true ^ (<"+root.text.strip()+">)")
		else:
			fhw.write("true ^ ({{"+root.text.strip()+"}}) ^ ")
	elif root.tag in ["receive", "rack", "condref"]:
		fhw.write("true ^ (<"+root.attrib["idref"]+">)"+("" if isLast and isLastEvent(cur_inst, root) else " ^ "))
	elif root.tag == "invariant":
		get_invar(root, listInvar)
	else:
		for child in root:
			build_formula(fhw, child, cur_inst, dictNom, dictTimer, listInvar, isLast)
	return None

def get_inst_param(root, paramL, nomL):
	if root.tag in ["transmit", "ack", "action", "condition", "timer"]:
		if root.tag in ["transmit", "ack", "condition"]:
			nomL.append(root.attrib["id"])
		if root.text.strip() not in paramL:
			paramL.append(root.text.strip())
	elif root.tag in ["receive", "rack", "condref"]:
		if root.attrib["idref"] not in nomL:
			nomL.append(root.attrib["idref"])
	else:
		for child in root:
			get_inst_param(child, paramL, nomL)
	return (paramL, nomL)


def isLastEvent(root, node):
#	print "def isLastEvent(root, node):", root, node
	if (root.tag in observableEvents) or (root.tag in ["simregion", "coregion"]):
		return (True if (root == node) and ("temp" in node.attrib) and (node.attrib["temp"] == "cold") else False)
	else:
		for i in range(len(root._children)-1, -1, -1):
			if root._children[i].tag != "invariant": 
				return isLastEvent(root._children[i], node)	


def update_dicttimer(dictTimer, node):
	timer = node.text.strip()
	if timer in dictTimer:
		dictTimer[timer].append(node.attrib["dur"])
	else:
		dictTimer[timer] = [node.attrib["dur"]]


def update_dictnominals(dictNom, node):
	nominal = node.attrib["id"]
	if nominal in dictNom:
		dictNom[nominal] += 1
	else:
		dictNom[nominal] = 1


def get_invar(node, listInvar):
	formula = "true ^ <"+node.attrib["start-idref"]+"> ^ [["+node.text.strip()+"]] ^ <"+node.attrib["end-idref"]+"> ^ true"
	listInvar.append((node.attrib["start-idref"], node.attrib["end-idref"], formula))


def sim_region(fhw, root, cur_inst, dictNom, dictTimer, listInvar, isLast):
#	print "def sim_region(fhw, root, isLast):"
	L = get_events_in_region(root, dictNom, dictTimer, listInvar)
	formula = "true ^ ("+reduce(lambda x, y:x+" && "+y, [z1+("{{"+z2+"}}" if z2 else "") for z1, z2 in L])
	if isLast and isLastEvent(cur_inst, root):
		formula += " || "+reduce(lambda x, y:x+" && "+y, [z1+("<"+z2+">" if z2 else "") for z1, z2 in L])+" && !ext"
		fhw.write(formula+")")
	else:
		fhw.write(formula+") ^ ")
	print "sim formula=", formula+")"
	

def coregion(fhw, root, cur_inst, dictNom, dictTimer, listInvar, isLast):
#	print "def coregion(fhw, root, isLast):", root.tag
	L = get_events_in_region(root, dictNom, dictTimer, listInvar)
	L1 = list(set(L))
	L2 = [w2 for w1, w2 in L if w2] 
	L3 = list(set(L2))
	Y = map(lambda x, y: (x, str(y)), L3, [L2.count(w) for w in L3])
	formula = "true ^ ("+reduce(lambda x, y: x+"(scount "+y[0]+" >= 1) && (scount "+y[0]+" <= "+y[1]+") && ", Y, "")
	s = reduce(lambda x, y: x+" || "+y, [z1+("{{"+z2+"}}" if z2 else "") for z1, z2 in L1]) 
#	s += " || ("+reduce(lambda x, y: x+" || "+y, [z1+("<"+z2+">" if z2 else "") for z1, z2 in L1])+") && !ext || pt"
	formula += "(("+(len(L)-1)*(s+") ^ (")+s
	if isLast and isLastEvent(cur_inst, root):
		formula += " || ("+reduce(lambda x, y: x+" || "+y, [z1+("<"+z2+">" if z2 else "") for z1, z2 in L1])+") && !ext || pt"
		fhw.write(formula+")))")
	else:
		fhw.write(formula+"))) ^ ")
#	print "coformula=", formula+")))"

def get_events_in_region(root, dictNom, dictTimer, listInvar):
#	print "def get_events_in_region(root):", root.tag
	L = list()
	for node in root:
		if node.tag in ["receive", "rack", "condref", "timeout"]:
			text = get_id_text(treeRoot, node.attrib["idref"])
			L.append(("<"+node.attrib["idref"]+">", ""))
		elif node.tag in ["transmit", "ack", "condition"]:
			L.append(("<"+node.attrib["id"]+"> && ", node.text.strip()))
			update_dictnominals(dictNom, node)
		elif node.tag == "invariant":
			get_invar(node, listInvar)
		else:
			L.append(("", node.text.strip()))
			if node.tag == "timer":
				update_dicttimer(dictTimer, node)
	return L


def get_id_text(node, idref):
#	print "get_id_text", node.tag, idref
	if node.tag in observableEvents:
		if node.tag in ["transmit", "ack", "condition", "timer"]:
			if node.attrib["id"] == idref:
				return node.text.strip()
	else:
		for cl in node:
			text = get_id_text(cl, idref)
			if text:
				return text



##    __main__

def main(fhw, root, macro, specletId):
	print "def process_msc.main(fhw, root, macro, specletId, paramL):"
	global treeRoot, observableEvents
	observableEvents = ["transmit","receive","ack","rack","action","condition","condref","timer","timeout"]
	dictPrintRE = dict()
	listChartId = list()
	treeRoot = root
	with open("print.template", "r") as fhtmp:
		for line in fhtmp:
			key, val = line.replace("\g","\n").replace("\h","\t")[:-1].split(":")
			dictPrintRE.update({key:val})
	fhtmp.close()
	return build_formula(fhw, root._children[0], None, dict(), None, None, None)
	

