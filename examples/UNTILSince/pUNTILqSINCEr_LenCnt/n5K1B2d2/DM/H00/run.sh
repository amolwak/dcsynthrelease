rm *.dfa *.tra *.lab *.rew *.scade *.lus *.soft* *.hard* *.ec stats.txt
qsf pUq.qsf --disable-parsing
synth2 pUq.hardreq.dfa pUq.softreq.dfa pUq.io foo scadeBounded 1 1 synth.config > pUq.scade
synth_deterministic IndicatorsProjectedOut.dfa pUq.determinize.io foo 0 > pUq.determinized.scade

