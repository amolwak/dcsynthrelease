#!/bin/python

import re, os

class codetree():
	def __init__(self, tag, attrib=dict()):
		self.tag = tag 
		self.attrib = attrib
		self.children = list()
		self.i = 0
		self.D = dict()

	def __iter__(self):
		self.i = 0
		return self

	def next(self):
		if self.i < len(self.children):
			self.i += 1
			return self.children[self.i-1]
		else:
			raise StopIteration


def parse(allCode):
#	print "def parse(H, fhName):", allCode
#	try:
#		open(fhName, "r")
#	except:
#		print "Error in opening file: "+fhName
#		exit(0)
#	codeL = list()
	allCode = tokenize_header(allCode)
#	print "here=", allCode
#	allCode = re.sub(r'{{([ \t]*[A-Za-z(!][\sA-Za-z0-9_%+-/\*\[\]&\|!\(\)]*)}}', r'~~\1$$', allCode)
#	print "here=", allCode
#	allCode = re.sub(r'{{([ \t]*[A-Za-z(!;])', r'~~\1', allCode)
#	print "here2=", allCode
#	allCode = re.sub(r'}}([ \t]*[&|;^)])', r'$$\1', allCode)
	allCode = re.sub(r'}}', r'$$', re.sub(r'{{', r'~~', allCode))
#	print "here3=", allCode
	allCode = re.sub(r'[\t]+', r' ', re.sub(r'[\n]', r'', re.sub(r'([\s;{}()]else)(?=[\s{])', r'\1 ;', allCode)))
#	i = 0
#	while(i < len(allCode)):
#		s = re.match(r'{{[\sA-Za-z0-9_%+-/\*\[\]&\|!\(\)]+}}', allCode[i:])
#		if s:
#			allCode = allCode[:i]+re.sub(r'{', r'~', re.sub(r'}', r'$', allCode[i:i+len(s.group(0))]))+allCode[i+len(s.group(0)):]
#			i += len(s.group(0))
##			print len(s.group(0))
#		else:
#			i += 1
	allCode = tokenize(allCode)
#	print "allCode=", allCode
#	allCode = tokenize_keyCat2(tokenize_keyCat(tokenize_keyCat(allCode, keyCat1L), keyCat4L)) 
##		print "line=", line.strip()
#		if not re.match(r'[\s]*$', line):
##			print "line2="+line.strip()+"*"
#	print "split=", re.split(r'({|}|;)', allCode)
	return map(lambda x: re.sub(r'\?', r';', re.sub(r'~', r'{', re.sub(r'\$', r'}', x.lstrip()))), re.split(r'({|}|;)', allCode))


def tokenize(allCode):
	allCode = tokenize_keyCat(allCode, keyCat1La)
	L = list()
	for keyword in keyCat1Lb: 
		L.append((r'[\s;{}()]'+keyword+'\(', r'.*?;.*?;.*?;.*?;.*?;.*?;.*?\)'))
		L.append((r'[\s;{}()]'+keyword+'\s', r'.*?\(.*?;.*?;.*?;.*?;.*?;.*?;.*?\)'))
	allCode = insert_semi(allCode, L)
#	print "here=", allCode
	allCode = tokenize_keyCat(allCode, keyCat4L)
	L = list()
	for keyword in keyCat2L: 
		L.append((r'[\s;{}()]'+keyword+'\s', r''))
	L.append((r'[\s;{}()]ind\s+.*?:', r''))
	L.append((r'[\s;{}()]sm\s+(concurrent\s+)?[A-Za-z][A-Za-z0-9_]*', r''))
	L.append((r'[\s;{}()]state\s+((initial|final)\s+)?((initial|final)\s+)?[A-Za-z][A-Za-z0-9_]*', r''))
	allCode = insert_semi(allCode, L)
#	allCode = tokenize_keyCat2(allCode) 
#	print "here2=", allCode
	return allCode
	

def tokenize_header(allCode):
#	print "def tokenize_header(allCode):", allCode
	s = re.match(r'(\n)*.*?".*?"', allCode)
	allCode = allCode[:len(s.group(0))]+";"+allCode[len(s.group(0)):]
	return insert_semi(allCode, [(r'#include\s+"', r'\s*.*?"'), (r'#include\s*<', r'\s*.*?>'), (r'#dclib\s+"', r'\s*.*?"'),  (r'#dclib\s*<', r'\s*.*?>'), (r'#define', r'.*?(?=\n)')])
	

def insert_semi(allCode, L):
#	print "def insert_semi(allCode, L):", allCode, L
	for keyword, regex in L:
		i = 0
		while(i < len(allCode)):
			s = re.match(keyword, allCode[i:])
			if s: 
				s2 = re.match(regex, allCode[i+len(s.group(0)):])
#				print "s=", s, allCode[i:]
#				print "s2=", s2, regex, re.match(regex, allCode[i+len(s.group(0)):]) #, keyword+"*"+allCode[i:]
				allCode = allCode[:i+len(s.group(0))]+re.sub(r';', r'?', s2.group(0))+";"+allCode[i+len(s.group(0)+s2.group(0)):]
				i += len(s.group(0)+s2.group(0))
			i += 1
	return allCode


def tokenize_keyCat(allCode, keyCat):
#	print "def match_paren(L):", allCode, keyCat
	for keyword in keyCat:
		i = 0
		while(i < len(allCode)):
			if re.match(r'[\s;{}()]'+keyword+'\(', allCode[i:]) or re.match(r'[\s;{}()]'+keyword+'\s', allCode[i:]): 
				(allCode2, j) = get_balanced_paren_exp(allCode[i+len(keyword)+1:])
				allCode = allCode[:i+1]+keyword+allCode2
				i += j
#			else:
			i += 1
#	return re.sub(r'\\\(', r'(', re.sub(r'\\\)', r')', allCode))
#	print allCode
	return allCode


def get_balanced_paren_exp(line2):
#	print "def get_balanced_exp(line):", line2.strip(), "*"+line2[0]+"*"
	parenDepth = -1
	for j in range(len(line2)):
#		if (j>0) and (line2[j-1] != "\\") and (line2[j] == "(") or (j==0) and (line2[j] == "("):
		if line2[j] == "(":
			parenDepth += 1 if parenDepth > 0 else 2 
#		if (line2[j-1] != "\\") and (line2[j] == ")"):
		elif line2[j] == ")":
			parenDepth -= 1 
			if parenDepth == 0:
				return (line2[:j+1].replace(";", "?")+";"+line2[j+1:], j)
			elif parenDepth < 0:
				return (line2[:j]+";"+line2[j:], j-1)
#	print "line2=", line2
	return (line2, len(line2))


#def tokenize_keyCat2(line):
##	print "def tokenize_keyCat2L(line, keyword):", line
#	for keyword in keyCat2L:
#		i = 0
#		if re.match(r''+keyword+'\s', line):
#			line = keyword+";"+line[len(keyword):]
#		while(i < len(line)):
#			if re.match(r'[\s;{}()]'+keyword+'\s', line[i:]): 
##				if any((re.match(r'[\s;{}()]'+key+'\(', line[i+len(keyword)+1:]) or re.match(r'[\s;{}()]'+keyword+'\s', line[i+len(keyword)+1:])) for key in keyCat1L): 
##				if is_keyCat1(line[i+len(keyword)+1:], ""):
#				line = line[:i]+keyword+";"+line[i+len(keyword)+1:]
#				i += len(keyword)
#			i += 1
##	print "line2=", line.strip()
#	return line
#

#def is_keyCat1(s):
##	print "def is_keyCat1:", s,
#	for key in keyCat1L:
#		if re.match(r'[\s;{}()]*'+key+'\(', s) or re.match(r'[\s;{}()]*'+key+'\s', s):
#			return True
#	return False
		

#def get_include_files(H, codeL):
#	for s in codeL:
#		if re.match(r'#include', s):
#			L = s.split()
#			if L[1].strip()[1:-1] not in H["include"]:
#				H["include"].append(L[1].strip()[1:-1])


def build_code_tree(parent, i, H):
#	print "def build_code_tree(codeL, H):", i, codeL[i]
	parent.children.append(create_tree(codeL[i], H))
	print "parent=", parent.tag, parent.children[-1].tag
#	create_tree(codeL[i], H)
	if non_singular(codeL[i]):
		depth = 0
		while(True):
			i += 1
			if codeL[i] == "{":
				depth += 1
#				print "depth=", depth, i
			elif codeL[i] == "}":
				depth -= 1
#				print "depth2=", depth, i
			else:
				i = build_code_tree(parent.children[-1], i, H)
			if depth == 0:
#				print "********************"
				return i
#	print "############################"
	return i

		
def create_tree(code, H):
#	print "def create_tree(s):", code
	L = code.split()
#	print L, L[0]
	if L[0] == "begin":
		return codetree(L[0])		 
	elif L[0] == "#qsf":
		return codetree(L[0], dict({"qsf": L[1]}))		 
	elif L[0] == "#define":
		return codetree(L[0], dict({"def": L[1], "val": L[2]}))		 
	elif "#include" in L[0]:
		if len(L) == 1:
			L = re.sub(r'<', r' <', L[0]).split()
		if L[1][1:-1].strip() not in H["include"]:
			H["include"].append(L[1][1:-1].strip())
		return codetree(L[0], dict({"include": L[1]}))		
	elif "#dclib" in L[0]:
		if len(L) == 1:
			L = re.sub(r'<', r' <', L[0]).split()
		return codetree(L[0], dict({"dclib": L[1]}))		
	elif L[0] in ["indicator", "input", "output", "auxvar", "dclib", "useind"]:
		return codetree(L[0], dict({"data": code.strip()[len(L[0]):]}))
#		return codetree(L[0], dict({"data": reduce(lambda x, y: x+y.strip()+", ", code.lstrip()[len(L[0]):].split(","), "")[:-2]}))
	elif L[0] in ["const", "constant"]:
		L2 = code.strip()[len(L[0]):].split(",")
#		print L2, dict({"data": reduce(lambda x, y: x+y[0].strip()+"="+y[1].strip()+", ", [z.split("=") for z in L2], "")[:-2]})
		return codetree(L[0], dict({"data": reduce(lambda x, y: x+y[0].strip()+"="+y[1].strip()+", ", [z.split("=") for z in L2], "")[:-2]}))
#	elif L[0] == "softreq":
##		print reduce(lambda x, y: x+y.strip()+" >> ", code.lstrip()[len(L[0]):].split(">>"), "")[:-4]
#		return codetree(L[0], dict()) #{"data": reduce(lambda x, y: x+y.strip()+" >> ", code.lstrip()[len(L[0]):].split(">>"), "")[:-4]}))
	elif L[0] in specL:
#		print get_dict_id_param(L[1:], "paramspec")
		return codetree(L[0], get_dict_id_param(L[1:], "paramspec"))
	elif L[0] in ["td", "qddc"]:
#		print "here=", L[0], L[1:]
		return codetree(L[0], get_dict_id_param(L[1:], "paramspeclet"))
	elif L[0] == "node":
		return codetree(L[0], get_dict_id_param(L[1:], "param"))
	elif L[0] in ["nominal", "var", "sig", "bc"]:
#		print "L1=", len(L[0]), code[len(L[0]):]
		return codetree(L[0], dict({"data": map(lambda x: x.strip(), code[len(L[0]):].split(","))}))
#		return codetree(L[0], dict({"data": reduce(lambda x, y: x+y.strip()+", ", L[1].split(","), "")[:-2]}))
	elif re.match(r'for\(', code) or re.match(r'for\s', code):
#		print code[3:]
#		print re.sub(r'^([^;]*;[^;]*)&&', r'\1and', re.sub(r'^([^;]*;[^;]*)\|\|', r'\1or', code[3:]))
		return codetree("for", dict({"exp": substitute_and_or(code[3:])}))
#		return codetree("for", dict({"exp": re.sub(r'^([^;]*;[^;]*)&&', r'\1and', re.sub(r'^([^;]*;[^;]*)\|\|', r'\1or', code[3:]))}))
	elif re.match(r'with\(', code) or re.match(r'with\s', code):
#		print re.sub(r'^([^;]*;[^;]*)&&', r'\1and', re.sub(r'^([^;]*;[^;]*)\|\|', r'\1or', code[4:]))
#		print "code=", substitute_and_or(code[4:])
		return codetree("with", dict({"exp": substitute_and_or(code[4:])}))
	elif re.match(r'withpar\(', code) or re.match(r'withpar\s', code):
#		print re.sub(r'^([^;]*;[^;]*)&&', r'\1and', re.sub(r'^([^;]*;[^;]*)\|\|', r'\1or', code[7:]))
		return codetree("withpar", dict({"exp": substitute_and_or(code[7:])}))
	elif re.match(r'if\(', code) or re.match(r'if\s', code):
#		return codetree("if", dict({"exp": re.sub(r'&&', r'and', re.sub(r'\|\|', r'or', code[2:])).strip(), "sm_exp": code[2:].strip()}))
		return codetree("if", dict({"exp": code[2:].strip()}))
#		return codetree("if", dict({"exp": re.sub(r'\s','',code[2:])}))
	elif re.match(r'elif\(', code) or re.match(r'elif\s', code):
		return codetree("elif", dict({"exp": re.sub(r'&&', r'and', re.sub(r'\|\|', r'or', code[4:])).strip(), "sm_exp": code[4:].strip()}))
#		return codetree("elif", dict({"exp": re.sub(r'\s','',code[4:])}))
	elif re.match(r'else\(', code) or re.match(r'else\s', code):
		return codetree("else", dict())
	elif re.match(r'printf\(', code) or re.match(r'printf\s', code):
#		print "here", code[7:]
		s = re.match(r'[\s]*".+(?<!=\\)(")', code[6:].strip()[1:]).group(0)
#		print re.match(r'(.*)\)', code[7+len(s):]).group(0)
#		print code[7+len(s):].strip()[1:-1].strip()
#		print dict({"fstring": re.sub(r'%[fdi]', r'%s', s), "data": code[7+len(s):].strip()[1:-1].strip()})
		return codetree("printf", dict({"fstring": s, "data": code[6+len(s):].strip()[1:-1].strip()}))
	elif L[0] == "ind":
#		print "codeind=", code, L[1].rstrip(":")
		return codetree(L[0], dict({"indicator": L[1].rstrip(":")}))
	elif L[0] in ["indef", "assume", "req"]:
		return codetree(L[0], dict())
	elif L[0] in ["inferG", "inferF", "inferGF", "inferFG", "interface", "definitions", "indefinitions", "hardreq", "softreq"]:
		return codetree(L[0])
	elif L[0] == "encode":
		return codetree(L[0], dict({"data": code.strip()[len(L[0]):]}))
	elif L[0] == "sm":
		return codetree(L[0], dict({"id": L[2] if L[1] == "concurrent" else L[1], "concurrent": True if L[1] == "concurrent" else False}))
	elif L[0] == "state":
		i = 2 if "initial" in L else 1 
		i += 1 if "final" in L else 0 
		D = dict({"initial": any(z == "initial" for z in L), "final" : any(z == "final" for z in L), "id": L[i]})
#		print "D2=", D
		return codetree(L[0], D)
	elif L[0] in ["onentry", "onexit", "unless", "until"]:
		return codetree(L[0], dict())
#	elif L[0] in ["var", "sig"]:
#		return codetree(L[0], dict())
	else:
#		if is_fn(code):
#			return codetree("function", get_dict_id_param(code, "param"))
#		else:
#		print "code=", code #, get_dict_id_param(code)
		return codetree("singular", dict({"data": re.sub(r'==', r'=', code)}))


def substitute_and_or(s):
#	print "def substitute_and_or(s):", s
	while re.search(r'^([^;]*;[^;]*)\|\|', s):
		s = re.sub(r'^([^;]*;[^;]*)\|\|', r'\1or', s)
	while re.search(r'^([^;]*;[^;]*)&&', s):
		s = re.sub(r'^([^;]*;[^;]*)&&', r'\1and', s)
	for i in range(3,6):
		while re.search(r'^([^;]*'+i*";[^;]*"+')==', s):
			s = re.sub(r'^([^;]*'+i*";[^;]*"+')==', r'\1=', s)
#		print "s=", s
#	print "here=", s
	return s
	
			
def get_dict_id_param(fL, param):
#	print "def get_dict_id_param(fL, param):", fL, param
	L = re.split(r'(\(|\))', reduce(lambda x, y: x+y, fL))
	parameters = "("+reduce(lambda x, y: x+y.strip()+", ", L[2].split(","), "")[:-2]+")"
#	print "param = ", "("+reduce(lambda x, y: x+y+",", L[2].split(","), "")[:-2]+")"
	return dict({"id": L[0], param: parameters})
#		print L
			 
#	print s	


def non_singular(code):
#	print "def non_singular(code):", code
	L = code.split()
#	print "*"+L[0]+"*", True if any((re.match(r''+keyword+'\(', L[0]) or re.match(r''+keyword+'\s', L[0]) or (L[0] == keyword)) for keyword in decL+keyCat1La+keyCat1Lb+keyCat2L+keyCat4L+keyCat5L+keyCat7L+specL) else False
	return True if any((re.match(r''+keyword+'\(', L[0]) or re.match(r''+keyword+'\s', L[0]) or (L[0] == keyword)) for keyword in decL+keyCat1La+keyCat1Lb+keyCat2L+keyCat4L+keyCat5L+keyCat7L+specL) else False


#def is_fn(s):
#	return True if re.match(r'[A-Za-z0-9]+\(', s) else False		


	
	
def reconstitute_if_paren(codeL, i):
#	print "def reconstitute_if_paren(codeL, i, depth):", codeL[i].strip()
	while(i < len(codeL)):
#		print i, codeL[i], "len(codeL)=", len(codeL)
#		print any((re.match(r''+keyword+'\(', codeL[i]) or re.match(r''+keyword+'\s', codeL[i])) for keyword in keyL)
		if any((re.match(r''+keyword+'\(', codeL[i]) or re.match(r''+keyword+'\s', codeL[i])) for keyword in ["if", "elif", "else"]): 
#			print "here", codeL[i]
			codeL.insert(i+1, "{")
			i = reconstitute_if_paren(codeL, i+2)
#			print "here3", codeL[i]
#			print re.match(r'else\(', codeL[i]), re.match(r'else', codeL[i]), codeL[i]
			while(any((re.match(r''+keyword+'\(', codeL[i]) or re.match(r''+keyword+'\s', codeL[i])) for keyword in ["if", "elif", "else"])):
				i = reconstitute_if_paren(codeL, i)
#			print "i=", i, codeL[i]
			codeL.insert(i, "}")
#			print "ifreturn3=", i, len(codeL)
#			print "here4=", codeL[i+1].strip(), codeL[i+2].strip()	
			return i+1
		elif any((re.match(r''+keyword+'\(', codeL[i]) or re.match(r''+keyword+'\s', codeL[i])) for keyword in keyCat1La+keyCat1Lb) or non_singular(codeL[i]):
			return reconstitute_if_paren(codeL, i+1)
		elif codeL[i] == "{":
#			print "up", i
			i += 1
#			print "up=", codeL[i].strip()
			while(codeL[i] != "}"):
#				print "in while loop"
				i = reconstitute_if_paren(codeL, i)
#			print "up2=", codeL[i].strip(), codeL[i+1].strip()
#			print "exit up2", i, len(codeL)
			return i+1
		elif codeL[i] == "}":
			return i
		else:
#			print "codeL[i]=", codeL[i]
			return i+1
	return i	


#
#def print_code(root, depth, cropped, Href):
##	print "parent=", root.tag, depth
#	if cropped:
#		if root.tag in specL and root.attrib["id"] not in Href:
#			return
#	if root.tag in ["inferG", "inferF", "inferGF", "inferFG", "interface"]:
#		print root.tag
#		print "{"
#		depth += 1
#	elif root.tag == "#qsf":
#		print "#qsf "+root.attrib["qsf"]+"\n"
#	elif root.tag == "#define":
#		print "#define "+root.attrib["def"]+" "+root.attrib["val"]+"\n"
#	elif root.tag == "#include":
#		print "#include "+root.attrib["include"]+"\n"
#	elif root.tag in specL:
#		print "\t"*depth+root.tag+" "+root.attrib["id"]+root.attrib["paramspec"]
#		print "\t"*depth+"{"
#		depth += 1
#	elif root.tag == "nominal":
##		print root.tag, root.attrib["data"]
#		print "\t"*depth+root.tag+" "+reduce(lambda x, y: x+y+", ", root.attrib["data"], "")[:-2]+";"		
#	elif root.tag in keyCat3L:
#		print "\t"*depth+root.tag+" "+root.attrib["data"]+(";\n" if root.tag == "const" else ";")
##	elif root.tag == "const":
##		print "const "+root.attrib["data"]+";\n"
##	elif root.tag in ["input", "output", "constant", "auxvar", "dclib"]:
##		print "\t"*depth+root.tag+" "+root.attrib["data"]+";"
#	elif root.tag == "softreq":
#		print "\t"*depth+root.tag
#		depth += 1
#	elif root.tag in ["for", "with", "withpar", "iter", "if"]:
#		print "\t"*depth+root.tag+root.attrib["exp"]
#		print "\t"*depth+"{"
#		depth += 1
##	elif root.tag == "if":
##		print "\t"*depth+root.tag+root.attrib["exp"]
##		print "\t"*depth+"{"
##		depth += 1
#	elif root.tag in ["elif", "else"]:
#		print "\t"*(depth-1)+"}"
#		print "\t"*(depth-1)+root.tag+(root.attrib["exp"] if root.tag == "elif" else "")
#		print "\t"*(depth-1)+"{"
##	elif root.tag == "else":
##		print "\t"*(depth-1)+"}"
##		print "\t"*(depth-1)+root.tag
##		print "\t"*(depth-1)+"{"
#	elif root.tag in ["ind", "assume", "req"]:
#		print "\t"*depth+root.tag+" ",
#		for child in root:
#			print_code(child, 0, cropped, Href)
##	elif root.tag == "function":
##		print "\t"*depth+root.attrib["id"]+root.attrib["param"]+";"
#	elif root.tag == "singular":
#		print "\t"*depth+root.attrib["data"]+";"
#	elif root.tag in ["td", "qddc"]:
#		print "\t"*depth+root.tag+" "+root.attrib["id"]+root.attrib["paramspeclet"]
#		print "\t"*depth+"{"
#		depth += 1
#	elif root.tag == "printf":
#		print "\t"*depth+root.tag+"("+root.attrib["fstring"]+", "+root.attrib["data"]+");"
#	if root.tag not in ["ind", "assume", "req"]:
#		for child in root:
##			print "parent=", root.tag, 
#			print_code(child, depth, cropped, Href)
#	if (root.tag in decL+keyCat1La+keyCat1Lb+keyCat4L+keyCat7L+specL) and (root.tag != "elif") and (root.tag != "begin"):
#		print "\t"*(depth-1)+"}\n"
#





#######          __main__


def main(H, Hfile, allCode):
#	print "build_tree.main(H):", Hfile
	global decL, keyCat1La, keyCat1Lb, keyCat2L, keyCat3L, keyCat4L, keyCat5L, keyCat6L, keyCat7L, specL, codeL
	decL = ["inferG", "inferF", "inferGF", "inferFG", "begin", "interface", "definitions", "indefinitions", "hardreq", "softreq", "encode"]
#	keyCat1L = ["for", "with", "withpar", "if", "elif"] 
	keyCat1La = ["for", "if", "elif"] 
	keyCat1Lb = ["with", "withpar"] 
	keyCat2L = ["indef", "assume", "req", "onentry", "onexit", "unless", "until"] 
	keyCat3L = ["const", "indicator", "input", "output", "constant", "auxvar", "dclib", "useind", "nominal", "var", "sig", "bc", "emit"]
#	keyCat3La = ["nominal"]
	keyCat4L = ["td", "qddc", "node"]
	keyCat5L = ["else"]
	keyCat6L = ["#qsf", "#define", "#include", "#dclib"]
	keyCat7L = ["sm", "state", "ind"]
	ioFunctionsL = ["printf"]
	specL = ["init", "anti", "triggers", "implies", "follows", "pref", "ppref", "dc", 
		"#init", "#anti", "#triggers", "#implies", "#follows", "#pref", "#ppref", "#dc"]
#	codeL = parse(H, allCode)
#	codeL = ["begin", "{"]+filter(lambda x: (x != "\n") and  (x != '') and (x != ";"), [z.lstrip() for z in codeL])+["}"]
	codeL = ["begin", "{"]+filter(lambda x: (x != "\n") and  (x != '') and (x != ";"), [z.lstrip() for z in parse(allCode)])+["}"]
#	print codeL
#	get_include_files(H, codeL)	
#	print codeL
	reconstitute_if_paren(codeL, 0)	
#	print codeL
	build_code_tree(Hfile, 0, H)
#	print "returning build_tree.main"

