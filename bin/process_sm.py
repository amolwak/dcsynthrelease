#!/usr/bin/python3.3

import xml.etree.ElementTree as ET
import sys, copy, Queue, re


class infix:
	def __init__(self, op, left=None):
		self.left = left
		self.right = None
		self.v = op

MAXVAL = 4

def process_sm(root, C, D, E):
#	print "def process_sm(root, C, D, E):", root.tag #, (root.attrib["id"] if "id" in root.attrib else None)
	if root.tag in ["node", "sm"]:
#		root.attrib["ancestors"] = (root.attrib["parent"].attrib["ancestors"] if root.attrib["parent"] is not None else [])+[root.attrib["id"]]
#		print "root.attrib=", root.attrib
		for child in root:
			process_sm(child, C, D, E)
#	elif root.tag == "sm":
#		root.attrib["ancestors"] = (root.attrib["parent"].attrib["ancestors"] if root.attrib["parent"] is not None else [])+[root.attrib["id"]]
#		print "D=", root.attrib["id"], root.attrib["ancestors"]
#		for child in root:
#			process_sm(child, C, D, E)
	elif root.tag == "state":
#		root.attrib["ancestors"] = (root.attrib["parent"].attrib["ancestors"] if root.attrib["parent"] is not None else [])+[root.attrib["id"]]
#		print "D=", root.attrib["id"], root.attrib["ancestors"]
		for child in root:
			if child.tag == "singular":
				if "dataflow" not in root.attrib:
					root.attrib["dataflow"] = list()
				root.attrib["dataflow"].append(process_sm(child, C, D, E))
			else:
				process_sm(child, C, D, E)
	elif root.tag in ["onentry", "onexit"]:
		root.attrib[root.tag] = list()
		for child in root:
			root.attrib[root.tag].append(process_sm(child, C, D, E))
	elif root.tag in ["unless", "until"]:
		L = list()
		if root.tag not in root.attrib:
			root.attrib[root.tag] = list()
		for child in root:
			L.append([z.strip() for z in re.split(r':', child.attrib["data"])])
#			print root.tag, child.attrib["data"]
#			print "L[-1][0]=", L[-1][0]
			L[-1][0] = get_formula(child, L[-1][0])
#			print "L[-1][0]2=", L[-1][0]
			formula = "("+L[-1][0]+")"+((" && !("+" || ".join([L2[0].strip() for L2 in L[:len(L)-1]])+")") if len(L) > 1 else "")
#			print "formula=", formula, root.attrib["parent"].attrib["id"]
			L2 = L[-1][-1].split()
			L[-1][1] = " && ".join(map(lambda x: adjust_var(root, x), re.split(r'\s*&&\s*', re.sub(r'(^emit\s+|(?<=[\s&])emit\s+)', r'', L[-1][1]))))
#			print "L[-1][1]=", map(lambda x: adjust_var(root, x), re.split(r'\s*&&\s*', re.sub(r'(^emit\s+|(?<=[\s&])emit\s+)', r'', L[-1][1])))
			formula += " => "+(adjust_var(root, L[-1][1])+" && " if L[-1][1] else "")+L2[0]+"_"+get_state(root, L2[-1])
#			print root.attrib["parent"].attrib["id"], get_state(root, L[-1][-1])
			root.attrib[root.tag].append(formula)
#			print "formula", L[-1][1], formula
	elif root.tag in ["var", "sig", "bc"]:
#		print "root.attrib[data]=", root.attrib["data"]
		root.attrib["parent"].attrib[root.tag] = root.attrib["data"] #[z.strip() for z in re.split(r'[,;]', root.attrib["data"])]
#		print "L=", root.attrib["parent"].attrib
#		ancestors = ("_".join(root.attrib["parent"].attrib["ancestors"])+"_") if root.attrib["parent"].attrib["parent"] is not None else ""
#		print "ancestors=", map(lambda x: ancestors+x, root.attrib["parent"].attrib[root.tag])
#		eventD[root.tag].extend(root.attrib["parent"].attrib[root.tag])
	elif root.tag == "if":
		root.attrib["consequent"] = list()
		root.attrib["elif"] = list()
		if root.tag == "if":
			D2 = dict({"antecedent": [get_formula(root, root.attrib["exp"])]})
			root.attrib["antecedent"] = D2["antecedent"][-1]
		for child in root:
			if child.tag in ["singular", "if"]:
				root.attrib["consequent"].append("("+process_sm(child, C, D2, E)+")")
			else: 
				root.attrib["elif"].append(process_sm(child, C, D2, E))
#		print "L=", L
#		print "consequent=", root.attrib["consequent"]
#		print root.tag, " && ".join(root.attrib["consequent"])
#		print root.tag, root.attrib["exp"], "("+root.attrib["antecedent"]+" => "+" && ".join(root.attrib["consequent"])+")"+(" && "+" && ".join(root.attrib["elif"]) if root.attrib["elif"] else "")
		return "("+root.attrib["antecedent"]+" => "+" && ".join(root.attrib["consequent"])+")"+(" && "+" && ".join(root.attrib["elif"]) if root.attrib["elif"] else "")
	elif root.tag in ["elif", "else"]:
		root.attrib["consequent"] = list()
		if root.tag == "elif":
			D["antecedent"].append(get_formula(root, root.attrib["exp"]))
			root.attrib["antecedent"] = " && ".join(["!"+z for z in D["antecedent"][:-1]]+D["antecedent"][-1:]) 
		else:
			root.attrib["antecedent"] = " && ".join(["!"+z for z in D["antecedent"]])
		for child in root:
#			if child.tag == "singular":
			root.attrib["consequent"].append("("+process_sm(child, C, D2 if root.tag == "if" else D, E)+")")
#		print "L=", L
#		print "consequent=", root.attrib["consequent"]
		print root.tag, root.attrib["antecedent"]+" => "+" && ".join(root.attrib["consequent"])
#		return "("+root.attrib["antecedent"]+" => "+" && ".join(root.attrib["consequent"])+")"
	elif root.tag == "singular":
#		print "singular", root.attrib["data"], "****", get_formula(root, root.attrib["data"])
		return get_formula(root, root.attrib["data"])
#	else:
#		for child in root:
#			process_sm(child, C, D, E)


def get_state(root, state):
#	print "def get_state(root, state):", root.tag,root.attrib["parent"].attrib["ancestors"], root.attrib["parent"].attrib["parent"].attrib["peer_states"], state
	root2 = copy.deepcopy(root)
	while(root2.attrib["parent"] != None):
		root2 = root2.attrib["parent"]
#	print parent.tag, parent.attrib
#	print "recurse_and_find_state(root, state):", recurse_and_find_state(root, state).attrib["ancestors"], state
	return "_".join(recurse_and_find_state(root2, state).attrib["ancestors"])
#	if state in parent.attrib["parent"].attrib["peer_states"]:
#		return "_".join(parent.attrib["ancestors"][:-1])+"_"+state
#	else:
#		return "_".join(parent.attrib["ancestors"][:-3])+"_"+state

def recurse_and_find_state(root, state):
	if root.attrib["id"] == state:
#		print "returning recurse_and_find_state(root, state):", root.attrib["id"], state
		return root
	else:
		for child in root:
			if child.tag in ["state", "sm"]:
				r = recurse_and_find_state(child, state)
				if r:
					return r
				

def convert_to_dc(s):
	L = re.split(r'([A-Za-z][A-Za-z0-9_]*)', s)
#	print "L=", L
	for i in range(len(L)):
		if re.match(r'[A-Za-z][A-Za-z0-9_]*', L[i]) and (L[i] not in ["true", "false"]):
			L[i] = "<"+L[i]+">"
	return reduce(lambda x, y: x+y, L)


def get_formula(root, s):
#	print "def get_formula(s):*"+s+"*"
	L = filter(lambda x: x, [z.strip() for z in re.split(r'(=|->|\(|\)|&&|\|\||!|(?<=[=\s(])pre(?=[\s(])|(?<=[=\s(])last(?=[\s(])|^pre(?=[\s(])|^last(?=[\s(]))', s)])
	start = (max(i for i in range(len(L)) if L[i] == "=")+1) if (len(L) >= 2) and (L[1] == "=") else 0 
#	print "L=", L #, max(i for i in range(len(L)) if L[i] == "=")
#	print "infix_tree"	
#	print_infix_tree(infix_tree)
#	formula = build_formula(build_infix([infix("(")]+[infix(z) for z in L[start:]]+[infix(")")], list()))
#	print "formula=", formula
	formula = build_formula2(convert_to_postfix(L))
	L2 = filter(lambda x: x, [z.strip() for z in re.split(r'(=>|-|\(|\)|&&|\|\||!)', "("+formula+")")])
	remove_unwanted_paren(L2, 0)
	L2 = filter(lambda x: (x != "((") and (x != "))"), L2)
#	print "L2=", L2
	for i in range(len(L2)):
		if L2[i] not in ["||", "&&", "=>", "-", "!", "st", "true", "false", "(", ")"]:
			L2[i] = adjust_var(root, L2[i])
#	print "<"+adjust_var(root, L[0])+" <=> "+reduce(lambda x, y: x+y, L2, "")+">"  
#	return (adjust_var(root, L[0])+" <=> " if start == 2 else "")+"("+reduce(lambda x, y: x+y, L2, "")+")"
#	print "formula=", " <=> ".join(["<"+adjust_var(root, L[i])+">" for i in range(0, start, 2)])+(" <=> " if start else "")+reduce(lambda x, y: x+y, L2, "")
	return " <=> ".join(["<"+adjust_var(root, L[i])+">" for i in range(0, start, 2)])+(" <=> " if start else "")+reduce(lambda x, y: x+y, L2, "")
#	print "<"+adjust_var(root, L[0])+" <=> "+build_formula(build_infix([infix("(")]+[infix(z) for z in L[2:]]+[infix(")")], list()))+">"
#	return "true" #"<"+adjust_var(root, L[0])+" <=> ("+build_formula(build_infix(L[2:], list()))+")>"


def convert_to_postfix(infixL):
#	print "def convert_to_postfix(infixL):", infixL
#	prefD2 = dict({"pre": 3, "last": 3, "!": 3, "->": 2, "&&": 2, "||": 1})
#	infixL = ["("]+infixL+[")"]
	stack = ["("]
	output = list()
	for s in infixL+[")"]:
#		print stack
		if s in prefD.keys():
			while((stack[-1] in prefD.keys()) and (prefD[stack[-1]] >= prefD[s])):
				output.append(stack[-1])
				del stack[-1]
			stack.append(s)
		elif s == "(":
			stack.append(s)
		elif s == ")":
			while(stack[-1] != "("):
				output.append(stack[-1])
				del stack[-1]
			del stack[-1]
		else:
			output.append(s)
#	if stack:
#		output += stack.reverse()
	return output					


def build_formula2(output):
#	print "def build_formula2(output):", output
	output2 = list()
	for s in output:
		if s == "pre":
			output2[-1] = "(-"+output2[-1]+")" 
		elif s == "last":
			output2[-1] = "(-"+output2[-1]+")" 
		elif s == "!":
			output2[-1] = "(!"+output2[-1]+")" 
		elif s in ["&&", "||"]:
			output2[-2] = "("+output2[-2]+" "+s+" "+output2[-1]+")"
			del output2[-1]
		else:
			output2.append(s)
#		print output2
	return output2[0]
 




def adjust_var(root, v):
#	print "def adjust_var(root, v):", v
	for s in ["var", "sig"]:
		for s2 in ["state", "sm"]:
			root2 = root
			while(root2.attrib["parent"] != None):
				root2 = get_parent_sm_or_state(root2.attrib["parent"], s2) 
#				print "two roots", root2.attrib if root2 != None else None, root.attrib
#				print "here", v, s, s2, root2.attrib, root2.attrib[s] if (s in root2.attrib) and (v in root2.attrib[s]) else None
				if (s in root2.attrib) and (v in root2.attrib[s]):
					return "_".join(root2.attrib["ancestors"])+"_"+v
			if (s in root2.attrib) and (v in root2.attrib[s]) or ("bc" in root2.attrib) and (v in root2.attrib["bc"]):
				return "_".join(root2.attrib["ancestors"])+"_"+v
	return v


def get_parent_sm_or_state(root, sm_or_state):
#	print "def get_parent_sm_or_state(root, sm_or_state):", root.attrib["id"] if "id" in root.attrib else root.tag, sm_or_state
	return root if (root.attrib["parent"] == None) or (root.tag == sm_or_state) else get_parent_sm_or_state(root.attrib["parent"], sm_or_state)


def remove_unwanted_paren(L, start):
#	print "def rename_unwanted_paren(L, start):", start, L[start:]
	i = start + 1  
	pref = MAXVAL
	while((i < len(L)) and (L[i] != ")")):
#		print "in while loop", i, L[i:]
		if L[i] == "(":
			i, pref2 = remove_unwanted_paren(L, i)
			pref = min(pref, pref2)
#			print "i=", i, pref, pref2
		else:
			if L[i] in ["!", "-", "&&", "||", "=>"]:
				pref = min(pref, get_pref_paren(L, i))
			i += 1
#	print "here=", L[i], start, i, L[start:i+1]
	if (pref >= get_pref_paren(L, start-1)) and (pref >= get_pref_paren(L, i+1)):
		L[start] = "(("
		L[i] = "))"
		return i+1, pref
#	print lPref, get_pref_paren(L, start-1), rPref, get_pref_paren(L, i+1),
#	print (lPref >= get_pref_paren(L, start-1)) and (rPref >= get_pref_paren(L, i+1)), L[i+1]
	return i+1, MAXVAL


def get_pref_paren(L, i):
#	print "def get_pref_paren(L, i):", L[i]
	return (-1 if (i < 0) or (i >= len(L)) or (L[i] in ["(", ")"]) else dict({"!": 3, "-": 3, "&&": 2, "||": 1, "=>": 0})[L[i]])


#def build_infix(infixL, stack):
#	print "def get_infix(infixL, stack):", len(infixL)
#	while(len(infixL) > 1):
#		i = 0
#		stack = list()
#		infixL = [infix("(")] + infixL + [infix(")")]
#		l = len(infixL)	
#		print "infixL[i]=", i, infixL[i].v, len(infixL)
#		while(i < l):
#			if infixL[i].v == ")":
##				print "parent=", stack[-2].v, stack[-1].v
##				if stack[-2].v == "(":
##					stack[-2] = stack[-1]
##					del stack[-2]
##					print_stack(stack, "1b")
#				if (stack[-2].v != "(") and (stack[-1].v != ")"):
#					print_stack(stack, "1a")
#					stack[-2].right = stack[-1]
#					del stack[-1]
#					print_stack(stack, "1")
#				if stack[-2].v == "(":
#					del stack[-2]
#					reduce_stack(stack)
#				else:
#					stack.append(infixL[i])
#					
#				print_stack(stack, "2")
#			else:
#				stack.append(infixL[i])
#				print_stack(stack, "3")
#				if infixL[i].v != "(":
#					reduce_stack(stack)
#			i += 1
#		infixL = copy.deepcopy(stack)
##		print "stack=",
##		for s in stack:
##			print s.v,
##		print 
##	while(len(stack) > 1):
##		stack[-2].right = stack[-1]
##		del stack[-1]
##	print "returning from build_infix"
#	return stack[0]
#
#
#def print_stack(stack, n):
#	print "stack"+n+"=",
#	for s in stack:
#		print s.v,
#	print 
#
#
#def reduce_stack(stack):
#	print "def reduce_stack(stack):", stack[-1].v, len(stack)
#	i = get_prev_operator(stack)
##	print "i=",i
#	if i != None:
#		print "here=", stack[i].v, stack[-1].v#, get_pref(stack[i]), get_pref(stack[-1])
#		if (get_pref(stack[i]) == MAXVAL-1) and stack[-1].v not in ["!", "last", "pre"]:
#			print "here3"
#			stack[i].right = stack[-1]
#			del stack[-1]
#			reduce_stack(stack)
#		elif stack[-1].v in prefD.keys():
#			if get_pref(stack[i]) >= get_pref(stack[-1]):
#				print "here2=", stack[i].v, stack[-1].v
##				print "in stack=", i, len(stack)-1, stack[i-1].v
##				print_stack(stack, "4")
#				if (i > 0) and (stack[i-1].v != "("):
#					if (i == len(stack) - 2) and (get_pref(stack[i-1]) >= get_pref(stack[-1])):
#						i -= 1
#				if i < len(stack)-2:
#					stack[i].right = stack[i+1]
#				stack[-1].left = stack[i]
#				stack[i] = stack[-1]
#				del stack[i+1:]
##				print_stack(stack, "5")
#				reduce_stack(stack)
##			elif get_pref(stack[-1]) == MAXVAL:
##				print stack[-1].v
##				stack[i].right = stack[-1]
##				del stack[-1]
##				print_stack(stack, "6")
##				reduce_stack(stack)
##		elif get_pref(stack[-1]) == MAXVAL:
##			print "precedence=", prefD[stack[i].v], prefD[stack[-1].v]
##			stack[i].right = stack[-1]
##			del stack[-1]
##		reduce_stack(stack)
##		else:
##			stack[-2].right = stack[-1]
##			del stack[-1]
#	elif stack[-1].v in ["->", "&&", "||"] and stack[-1].left == None:
#		stack[-1].left = stack[-2]
##		stack[-2] = stack[-1]
#		del stack[-2]
#

#def reduce_stack(stack):
#	print "def reduce_stack(stack):", stack[-1].v, len(stack)
#	i = get_prev_operator(stack)
##	print "i=",i
#	if i != None:
#		print "here=", stack[i].v, stack[-1].v#, get_pref(stack[i]), get_pref(stack[-1])
#		if (get_pref(stack[i]) == MAXVAL-1) and stack[-1].v not in ["!", "last", "pre"]:
#			stack[i].right = stack[-1]
#			del stack[-1]
#			reduce_stack(stack)
#		elif stack[-1].v in prefD.keys():
#			if get_pref(stack[i]) >= get_pref(stack[-1]):
#				print "here2=", stack[i].v, stack[-1].v
##				print "in stack=", i, len(stack)-1, stack[i-1].v
##				print_stack(stack, "4")
#				if (i > 0) and (stack[i-1].v != "("):
#					if (i == len(stack) - 2) and (get_pref(stack[i-1]) >= get_pref(stack[-1])):
#						i -= 1
#				if i < len(stack)-2:
#					stack[i].right = stack[i+1]
#				stack[-1].left = stack[i]
#				stack[i] = stack[-1]
#				del stack[i+1:]
##				print_stack(stack, "5")
#				reduce_stack(stack)
#			elif get_pref(stack[-1]) == MAXVAL:
#				print stack[-1].v
#				stack[i].right = stack[-1]
#				del stack[-1]
#				print_stack(stack, "6")
#				reduce_stack(stack)
##		elif get_pref(stack[-1]) == MAXVAL:
##			print "precedence=", prefD[stack[i].v], prefD[stack[-1].v]
##			stack[i].right = stack[-1]
##			del stack[-1]
##		reduce_stack(stack)
##		else:
##			stack[-2].right = stack[-1]
##			del stack[-1]
#			
#	elif stack[-1].v in ["->", "&&", "||"] and stack[-1].left == None:
#		stack[-1].left = stack[-2]
#		stack[-2] = stack[-1]
#		del stack[-1]
	

			

def get_pref(infixInst):
#	print "def get_pref(infixInst):", infixInst.v, infixInst.left, infixInst.right
	if infixInst.v in ["->", "&&", "||"] and infixInst.left != None and infixInst.right != None:
		return MAXVAL
	elif infixInst.v in ["last", "pre", "!"] and infixInst.right != None:
		return MAXVAL
	else:
		return prefD[infixInst.v]


def get_prev_operator(stack):
	for i in range(len(stack)-2, -1, -1):
		if stack[i].v in ["->", "&&", "||", "pre", "last", "!"]:
			return i
		elif stack[i].v == "(":
			return None
	return None


#def print_infix_tree(tree):
#	if tree.left != None:
#		print_infix_tree(tree.left)
#	else:
#		if tree.right == None:
#			print tree.v
#			return
#	print tree.left.v if tree.left != None else None, tree.v, tree.right.v
#	if tree.right != None:
#		print_infix_tree(tree.right)
#	else:
#		print tree.v
#		return 

#
#def build_formula(infix_root, depth=0):
##	print "def build_formula(infix_root):", infix_root.v
#	if infix_root.v == "->":
#		return "(st => "+build_formula(infix_root.left)+") && (!st => "+build_formula(infix_root.right)+")"
#	elif infix_root.v in ["pre", "last"]:
#		if infix_root.right != None:
#			depth = (build_formula(infix_root.left, depth)+1) if infix_root.left != None else 1
#			return "-("*depth+build_formula(infix_root.right)+")"*depth
#		elif infix_root.left != None:
#			return build_formula(infix_root.left, depth)+1
#		else:
#			return 1
#	elif infix_root.v == "last":
#		return "-("+build_formula(infix_root.right)+")"
#	elif infix_root.v in ["&&", "||"]:
#		return "(("+build_formula(infix_root.left)+") "+infix_root.v+" ("+build_formula(infix_root.right)+"))"
#	elif infix_root.v == "!":
#		return "!"+build_formula(infix_root.right)
##	elif infix_root.v == ["(", ")"]:
##		return infix_root.v
#	else:
#		return infix_root.v #+(build_formula(infix_root.right) if infix_root.right != None else "")
#

def set_parent_and_ancestors(root, parent):
#	print "def set_parent_and_get_peer_states(root, parent):", (parent.tag if parent is not None else None), root.tag, id(root.attrib)
	root.attrib["parent"] = parent
	if root.tag == "node":
		root.attrib["ancestors"] = [root.attrib["id"]]
	elif root.tag in ["sm", "state"]:
		root.attrib["ancestors"] = root.attrib["parent"].attrib["ancestors"] + [root.attrib["id"]]
#	if root.tag == "sm":
#		root.attrib["peer_states"] = list() 
#		for child in root:
#			set_parent_and_get_peer_states(child, root, root.attrib["peer_states"])
#		print "peer states=", root.attrib["id"], root.attrib["peer_states"]
#	else:
#		if root.tag == "state":
#			peerStatesL.append(root.attrib["id"])
	for child in root:
		set_parent_and_ancestors(child, root)

	
#def print_parent(root):
##	print "def print_parent(root):", root.tag, root, root.attrib
#	if "parent" in root.attrib and root.attrib["parent"] is not None:
#		print root.tag, id(root), id(root.attrib)
##		if "id" in root.attrib:
##			print "here5", root.attrib["parent"].attrib, root.attrib["id"]
##		elif "id" in root.attrib["parent"].attrib:
##			print "here6", root.attrib["parent"].attrib, root.tag
##		else:
##			print "here4.1", root.attrib["parent"].tag, root.tag
#	for child in root:
#		print_parent(child)
#	





##    __main__

def main(root, F, Hconstant):
	print "def process_sc.main(fhw, root, macro, specletId):", root.tag
	global eventD, prefD
	eventD = dict({"var": list(), "sig": list(), "bc": list()})
	prefD = dict({"pre": 3, "last": 3, "!": 3, "->": 2, "&&": 2, "||": 1})
	set_parent_and_ancestors(root, None)
#	print "============================"
#	print_parent(root)	
	process_sm(root, dict(), dict({"ancestors": list()}), dict())
#	print "returning from process_sm.main()"
	return root, eventD
	

