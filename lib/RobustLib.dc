// Library Standardised to Cycles for K and B parameters.

#define EP(A) (true^<A>)
#define KBOUNDED(D,K) ((slen<K => (D)) && (true ^ slen=K => true ^ (slen=K && (D))))

-- The definitions of Error types ---
#define LocalErr(A) <!A>
#define ErrCount(A,K) (scount !A > K)
#define BurstErr(A,K) ([[!A]] && slen >= K)
#define HasBurstErr(A,K) (<>(BurstErr(A,K)))
#define HasNoRecovery(A,B) ([]([[A]] => slen < B-1))
#define ErrorWithNoRecovery(A,B,Error) (Error && HasNoRecovery(A,B))
#define SepErr(A,K) (((<!(-A)>)^[[A]]^(<+(!A)>)) && (slen < K-1))
#define HasSepErr(A,K) (<>(SepErr(A,K)))

-- The definitions for SCOPE of errors ---
#define NeverInPast(Error) (!<>(Error))
#define NeverInSuffix(Error) (!(true^(Error)))
#define NeverInPastLenLeq(B,Error) (!<>((slen <= B-1) && (Error)))
#define NeverInSuffixLenLeq(B,Error) (!(true^((slen<=B-1) && (Error))))

-- The definitions for Relaxed Assumptions
#define AssumeTrue(A) (true)
#define AssumeFalse(A) (false)
#define BeCorrect(A) (NeverInPast(LocalErr(A)))
#define BeCurrentlyCorrect(A) (NeverInSuffix(LocalErr(A)))
-- Resilient w.r.t. to error count (Intermittent and non-intermittent versions)
#define KBResCntInt(A,K,B) (NeverInSuffix(ErrorWithNoRecovery(A, B, ErrCount(A,K))))
#define KBResCnt(A,K,B) (NeverInPast(ErrorWithNoRecovery(A, B, ErrCount(A,K))))
-- Resilient w.r.t. to Burst error (Intermittent and non-intermittent versions)
#define KBResBurstInt(A,K,B) (NeverInSuffix(ErrorWithNoRecovery(A, B, HasBurstErr(A,K))))
#define KBResBurst(A,K,B) (NeverInPast(ErrorWithNoRecovery(A, B, HasBurstErr(A,K))))
-- KBVariant
#define KBLenCntInt(A,K,B) (NeverInSuffixLenLeq(B,ErrCount(A,K)))
#define KBLenCnt(A,K,B) (NeverInPastLenLeq(B,ErrCount(A,K)))
#define KBLenBurstInt(A,K,B) (NeverInSuffixLenLeq(B,HasBurstErr(A,K)))
#define KBLenBurst(A,K,B) (NeverInPastLenLeq(B,HasBurstErr(A,K)))
-- KBSepBurst (This is non intermittent only)
#define KBSepBurst(A,K,B) (NeverInPast(HasBurstErr(A,K) || HasSepErr(A,B)))
--Greedy
#define Greedy(A) (false)

