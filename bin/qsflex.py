# ----------------------------------------------------------------------
# clex.py
#
# A lexer for ANSI C.
# ----------------------------------------------------------------------

import sys
#sys.path.insert(0, "../ply")

import lex



reserved = [
	'HQSF', 'HINCLUDE', 'HDEFINE', 'HDCLIB', 'INFERG', 'INFERF', 'INFERGF', 'INFERFG', 'CONST', 
	'INTERFACE', 'DEFINITIONS', 'INDEFINITIONS', 'INDEF', 'HARDREQ', 'SOFTREQ', 'USEIND', 'ENCODE', 'INDICATOR', 
	'INPUT', 'OUTPUT', 'MONITOR', 'CONSTANT', 'AUXVAR', 'INVISIBLE', 'VISIBLE', 
	'IMPLIES', 'INIT', 'ANTI', 'TRIGGERS', 'FOLLOWS', 'PREF', 'PPREF', 'DC', 
	'HIMPLIES', 'HINIT', 'HANTI', 'HTRIGGERS', 'HFOLLOWS', 'HPREF', 'HPPREF', 'HDC',
	'NODE', 'VAR', 'SIG', 'BC', 'SM', 'STATE', 'INITIAL', 'FINAL', 'CONCURRENT', 
	'UNLESS', 'UNTIL', 'RESUME', 'RESTART', 
	'ONENTRY', 'ONEXIT', 'PRE', 'LAST', 'EMIT', 'ELSE', 
#	'FOR', 	'WITH',	'WITHPAR', 'IF', 'ELIF', 'ELSE',
	'TD', 'QDDC', 'NOMINAL',
	'IND', 'ASSUME', 'REQ'
]

reserved2 = ['FOR', 'WITH', 'WITHPAR', 'IF', 'ELIF', 'PRINTF']
#tokens = reserved + [
#	'ID', 'HASHID', 'ATSYNC', 'ATNULL', 'SCONST', 'ILLEGAL', 'FUNCTION', 
#    	'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MOD',
#	'OOR', 'AAND', 'NOT',
#	'AND', 'CARET', 'TILDE',
#   	'LT', 'LE', 'GT', 'GE', 'EQ', 'NE', 'NEG', 
#    	'EQUALS', 'TIMESEQUALS', 'DIVEQUALS', 'MODEQUALS', 'PLUSEQUALS', 'MINUSEQUALS',
#    	'PLUSPLUS', 'MINUSMINUS', 
#    	'LPAREN', 'RPAREN',
#    	'LBRACKET', 'RBRACKET',
#    	'LBRACE', 'RBRACE',
#    	'COMMA', 'SEMI', 'COLON', 
#    	'STUTTER', 
#	'NUMBER', 'TDVAL', 'XTDVAL', 'TRISTATE', 
#	'TRUE', 'FALSE',
#	'IMPLICATION', 'IFF', 
#	'MINUSGT', 
#	'SLEN', 'SCOUNT', 'SDUR',	
#	'PT', 'EXT', 'BOX', 'DIAMOND', 
#	'DGT', 'DLT', 'DLBRACKET', 'DRBRACKET', 'DLBRACE', 'DRBRACE'
#]

reserved3 = ['HASHID', 'ATSYNC', 'ATNULL', 'SCONST', 'ILLEGAL', 
	'AND', 'TILDE', 'NE', 
    	'EQUALS', 'RAISEEQUALS', 'TIMESEQUALS', 'DIVEQUALS', 'MODEQUALS', 'PLUSEQUALS', 'MINUSEQUALS',
    	'PLUSPLUS', 'MINUSMINUS', 
    	'LBRACE', 'RBRACE',
    	'SEMI', 'COLON', 
    	'STUTTER', 'MINUSGT', 'DGT', 'DLT'
]

valid_qddc_tokens = ['ID', 'FUNCTIONID', 'COMMA', 'DOT', 
	'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MOD', 'RAISE', 
	'OOR', 'AAND', 'NOT', 'CARET', 'NEG', 
	'LT', 'GT', 
	'LE', 'GE', 'EQ', 
	'ALL', 'EX', 'ALL1', 'EX1', 
    	'LBRACKET', 'RBRACKET',
    	'LPAREN', 'RPAREN',
	'NUMBER', 'TDVAL', 'XTDVAL', 'TRISTATE', 
	'TRUE', 'FALSE',
	'IMPLICATION', 'IFF', 
	'SLEN', 'SCOUNT', 'SDUR',	
	'PT', 'EXT', 'BOX', 'DIAMOND', 
	'DLBRACKET', 'DRBRACKET', 'DLBRACE', 'DRBRACE']

tokens = reserved + reserved2 + reserved3 + valid_qddc_tokens


reserved_map = dict()
for r in reserved:
	if r in ["HQSF", "HINCLUDE", "HDCLIB", "HDEFINE", "HIMPLIES", 'HINIT', 'HANTI', 'HTRIGGERS', 'HFOLLOWS', 'HPREF', 'HPPREF', 'HDC']:
		reserved_map["#"+r[1:].lower()] = r
	elif r in ["INFERG", "INFERF", "INFERGF", "INFERFG"]:
		reserved_map["infer"+r[5:].upper()] = r
	else:
		reserved_map[r.lower()] = r
#print reserved_map


#precedence = [
#	('left', 'ID'),
#	('left', 'AND', 'OR', 'IMPLICATION')
#]

t_ignore = ' \t\x0c'
# Operators
t_PLUS = r'\+'
t_MINUS = r'-'
t_RAISE = r'\*\*'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MOD = r'%'
#t_OR = r'\|\|'
#t_AND = r'&&'
t_STUTTER = r'\|(?!\|)'
t_LT = r'<'
t_GT = r'>'
t_LE = r'<='
t_GE = r'>='
t_EQ = r'=='
t_NE = r'!='
t_NEG = r'!(?!=)'
# Assignment operators
t_EQUALS = r'='
t_RAISEEQUALS = r'\*\*='
t_TIMESEQUALS = r'\*='
t_DIVEQUALS = r'/='
t_MODEQUALS = r'%='
t_PLUSEQUALS = r'\+='
t_MINUSEQUALS = r'-='
# Increment/decrement
t_PLUSPLUS = r'\+\+'
t_MINUSMINUS = r'--'
# Delimeters
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_COMMA = r','
t_SEMI = r';'
t_COLON = r':'
t_CARET = r'\^'
t_TILDE = r'~'
t_IMPLICATION = r'=>'
t_IFF = r'<=>'
t_MINUSGT = r'->'
t_DOT = r'\.'
t_SCONST = r'(\"([^\\\n]|(\\.))*?\"|\'([^\\\n]|(\\.))*?\')'



def t_NEWLINE(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


def t_TRUE(t):
	r'(?<=[\^\s(;])true(?=[\^\s);])'
	return t


def t_FALSE(t):
	r'(?<=[\^\s(;])false(?=[\^\s);])'
	return t


def t_BOX(t):
	r'\[\]'
	return t


def t_DIAMOND(t):
	r'<>'
	return t


def t_SLEN(t):
	r'(?<![A-Za-z0-9_])slen(?![A-Za-z0-9_])'
	return t


def t_SCOUNT(t):
	r'(?<![A-Za-z0-9_])scount(?=[\s])'
	return t


def t_SDUR(t):
	r'(?<![A-Za-z0-9_])sdur(?=[\s])'
	return t


def t_PT(t):
	r'(?<=[\s\^\(:{\|&>;])pt(?=[=\|&\s\^\);}])'
	return t


def t_EXT(t):
	r'(?<![A-Za-z0-9_])ext(?![A-Za-z0-9_])'
	return t


def t_TRISTATE(t):
    r'[012]+(?![0-9])'
    t.value = int(t.value)
    return t


def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t


def t_XTDVAL(t):
    r'x[012x]*'
    return t


def t_TDVAL(t):
    r'[012][012x]*'
    return t


def t_DGT(t):
	r'>>'
	return t


def t_DLT(t):
	r'<<'
	return t


def t_DLBRACKET(t):
	r'\[\['
	return t


def t_DRBRACKET(t):
	r'\]\](?!])'
	return t


def t_DLBRACE(t):
	r'{{'
	return t


def t_DRBRACE(t):
	r'}}'
	return t


def t_FOR(t):
	r'(?<=[\s{};])for\s*(?=\()'
	return t;


def t_WITH(t):
	r'(?<=[\s{};])with\s*(?=\()'
	return t;


def t_WITHPAR(t):
	r'(?<=[\s{};])withpar\s*(?=\()'
	return t;


def t_IF(t):
	r'(?<=[\s{};])if\s*(?=\()'
	return t;


def t_ELIF(t):
	r'(?<=[\s{};])elif\s*(?=\()'
	return t;


def t_PRINTF(t):
	r'(?<=[\s{};])printf\s*(?=\()'
	return t;


def t_FUNCTIONID(t):
	r'([A-Za-z][A-Za-z0-9_]*)(?=\()'
	return t;


def t_ALL(t):
	r'all'
	return t;


def t_EX(t):
	r'ex'
	return t;


def t_ID(t):
	r'[A-Za-z][A-Za-z0-9_]*'
    	t.type = reserved_map.get(t.value, "ID")
    	return t


def t_HASHID(t):
	r'\#[A-Za-z][A-Za-z0-9_]*'
    	t.type = reserved_map.get(t.value, "HASHID")
    	return t


def t_ATNULL(t):
	r'@null'
    	t.type = reserved_map.get(t.value, "ATNULL")
    	return t


def t_ATSYNC(t):
	r'@sync'
    	t.type = reserved_map.get(t.value, "ATSYNC")
    	return t


def t_AAND(t):
	r'&&'
	return t


def t_OOR(t):
	r'\|\|'
	return t


def t_AND(t):
	r'&'
	return t


def t_ILLEGAL(t):
	r'[@$?\'`]'
    	t.type = reserved_map.get(t.value, "ILLEGAL")
    	return t


def t_comment(t):
    r'/\*(.|\n)*?\*/'
    t.lexer.lineno += t.value.count('\n')


def t_cpp_comment(t):
    r'//.*\n'
    t.lexer.lineno += 1


# Preprocessor directive (ignored)


def t_preprocessor(t):
    r'\#(.)*?\n'
    t.lexer.lineno += 1


def t_error(t):
    print("Illegal character %s" % repr(t.value[0]))
    t.lexer.skip(1)


lexer = lex.lex()
if __name__ == "__main__":
    lex.runmain(lexer)
